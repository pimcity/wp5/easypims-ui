export const environment = {
  production: true,
  appVersion: 'v8.0.24',
  USERDATA_KEY: 'authf649fc9a5f55',
  isMockEnabled: true,
  apiUrl: 'api',
  appThemeName: 'Metronic',
  appPurchaseUrl: 'https://1.envato.market/EA4JP',
  appPreviewUrl: 'https://preview.keenthemes.com/metronic8/angular/demo1/',
  appPreviewAngularUrl: 'https://preview.keenthemes.com/metronic8/angular',
  appPreviewDocsUrl: 'https://preview.keenthemes.com/metronic8/angular/docs',
  appPreviewChangelogUrl:
    'https://preview.keenthemes.com/metronic8/angular/docs/changelog',
  // apiUrl: 'mysite.com/api'


  EasyPimsUrl: 'https://easypims.pimcity-h2020.eu', 
  PcmApiUrl: 'https://easypims.pimcity-h2020.eu/pcm-api',
  PdsApiUrl: 'https://easypims.pimcity-h2020.eu/pds/api',
  DpcApiUrl: 'https://easypims.pimcity-h2020.eu/portability',
  TtSecret: '02b2fedb-aacf-42f8-9005-c140e8cd5088',
  DpcSecret: 'e8bdee46-f8e7-4dfe-8cbb-5a89061ec509',
  TtApiUrl: 'https://easypims.pimcity-h2020.eu/privacy-metrics',
  UpApiUrl: 'https://easypims.pimcity-h2020.eu/ups-easypims',
  DaApiUrl: 'https://easypims.pimcity-h2020.eu/aggregation/api/data',
  SaApiUrl: 'https://easypims.pimcity-h2020.eu/ups-easypims',
  TmApiUrl: 'https://easypims.pimcity-h2020.eu/task-manager-api',
  EasypimsMail: 'dpo@pimcity-h2020.eu',
  DteApiUrl: 'https://easypims.pimcity-h2020.eu/dte-api',


};

import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { ClipboardModule } from 'ngx-clipboard';
import { TranslateModule } from '@ngx-translate/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './modules/auth/services/auth.service';
import { environment } from 'src/environments/environment';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import {ngfModule} from 'angular-file'
// #fake-start#
import { FakeAPIService } from './_fake/fake-api.service';
import { PersonalDataSafeComponent } from './pages/personal-data-safe/personal-data-safe.component';
import { PersonalConsentManagerComponent } from './pages/personal-consent-manager/personal-consent-manager.component';
import { PrivacyPreservingAnalyticsComponent } from './pages/privacy-preserving-analytics/privacy-preserving-analytics.component';
import { DataValuationToolsComponent } from './pages/data-valuation-tools/data-valuation-tools.component';
import { DataTradingEngineComponent } from './pages/data-trading-engine/data-trading-engine.component';
import { DataAggregationComponent } from './pages/data-aggregation/data-aggregation.component';
import { DataPortabilityControlComponent } from './pages/data-portability-control/data-portability-control.component';
import { DataProvenanceComponent } from './pages/data-provenance/data-provenance.component';
import { DataKnowledgeExtractionComponent } from './pages/data-knowledge-extraction/data-knowledge-extraction.component';
import { DataSourcesComponent } from './pages/data-portability-control/data-sources/data-sources.component';
import { DataTransformationsComponent } from './pages/data-portability-control/data-transformations/data-transformations.component';
import { DataExportsComponent } from './pages/data-portability-control/data-exports/data-exports.component';
import { ListNotWmComponent } from './pages/data-provenance/list-not-wm/list-not-wm.component';
import { ListWmComponent } from './pages/data-provenance/list-wm/list-wm.component';
import { PdsHomeComponent } from './pages/personal-data-safe/pds-home/pds-home.component';
import { PersonalInfoComponent } from './pages/personal-data-safe/personal-info/personal-info.component';
import { BrowsingHistoryComponent } from './pages/personal-data-safe/browsing-history/browsing-history.component';
import { LocationHistoryComponent } from './pages/personal-data-safe/location-history/location-history.component';
import { PdsHomeChartsComponent } from './pages/personal-data-safe/pds-home/pds-home-charts/pds-home-charts.component';
import { NgApexchartsModule } from 'ng-apexcharts';
// #fake-end#
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import {DpDatePickerModule} from 'ng2-date-picker';
import { TransparencyTagsComponent } from './pages/transparency-tags/transparency-tags.component';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';
import { NgxSpinnerModule } from "ngx-spinner";
import { PcmDashboardComponent } from './pages/personal-consent-manager/pcm-dashboard/pcm-dashboard.component';
import { PcmPreferencesComponent } from './pages/personal-consent-manager/pcm-preferences/pcm-preferences.component';
import { TtsTableComponent } from './pages/transparency-tags/tts-table/tts-table.component';
import { UserProfilingComponent } from './pages/user-profiling/user-profiling.component';
import { UpChartsComponent } from './pages/user-profiling/up-charts/up-charts.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { initializeKeycloak } from './init/keycloak-init.factory';
import { ShowAdsComponent } from './pages/show-ads/show-ads.component';
import { HomeComponent } from './pages/home/home.component';
import { MyDataComponent } from './pages/my-data/my-data.component';
import { MyConsentsComponent } from './pages/my-consents/my-consents.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { ReferralsComponent } from './pages/referrals/referrals.component';
import { SortTableDirective } from './directives/sort-table.directive';
import { SlickCarouselModule } from 'ngx-slick-carousel';


function appInitializer(authService: AuthService) {
  return () => {
    return new Promise((resolve) => {
      authService.getUserByToken().subscribe().add(resolve);
    });
  };
}

@NgModule({
  declarations: [AppComponent, PersonalDataSafeComponent, PersonalConsentManagerComponent, PrivacyPreservingAnalyticsComponent, DataValuationToolsComponent, DataTradingEngineComponent, DataAggregationComponent, DataPortabilityControlComponent, DataProvenanceComponent, DataKnowledgeExtractionComponent, DataSourcesComponent, DataTransformationsComponent, DataExportsComponent, ListNotWmComponent, ListWmComponent, PdsHomeComponent, PersonalInfoComponent, BrowsingHistoryComponent, LocationHistoryComponent, PdsHomeChartsComponent, TransparencyTagsComponent, PcmDashboardComponent, PcmPreferencesComponent, TtsTableComponent, UserProfilingComponent, UpChartsComponent, ShowAdsComponent, HomeComponent, MyDataComponent, MyConsentsComponent, SettingsComponent, PrivacyPolicyComponent, ReferralsComponent, SortTableDirective],
  imports: [
    BrowserModule,
    IvyCarouselModule,
    ngfModule,
    NgxDropzoneModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
    HttpClientModule,
    LeafletModule,
    ClipboardModule,
    DpDatePickerModule,
    BrowserModule,
    FormsModule,
    OwlDateTimeModule, 
    KeycloakAngularModule,
    NgxSpinnerModule,
    OwlNativeDateTimeModule,
    ReactiveFormsModule,
    SlickCarouselModule,
    // #fake-start#
    environment.isMockEnabled
      ? HttpClientInMemoryWebApiModule.forRoot(FakeAPIService, {
          passThruUnknownUrl: true,
          dataEncapsulation: false,
        })
      : [],
    // #fake-end#
    AppRoutingModule,
    InlineSVGModule.forRoot(),
    NgbModule,
    NgApexchartsModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

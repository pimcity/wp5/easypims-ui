import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ReceivedDatasets } from 'src/app/models/data-aggregation';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-data-aggregation',
  templateUrl: './data-aggregation.component.html',
  styleUrls: ['./data-aggregation.component.scss']
})

export class DataAggregationComponent implements OnInit {

  constructor(private modalService: NgbModal, private http: HttpService, private cd: ChangeDetectorRef) { }
  showTables: Boolean = false;
  receivedData: any;

  // Fake
  owners: string[] = [];
  createdAt: string[] = [];
  sensitive: boolean[] = [];

  ngOnInit(): void {
    this.http.daGetDatasets().subscribe(data =>{
      this.receivedData = data.datasets;
      this.fakeData();
      this.showTables = true;
      this.cd.detectChanges();
      console.log(this.receivedData);
    })
  }

  fakeData(){
    for(let i = 0; i<this.receivedData.length; i++){
      this.owners[i] = 'Owner ' + i.toString();    
      this.sensitive[i] = Math.random() < 0.5; 
      const date =  new Date(new Date(2012, 1, 1).getTime() + Math.random() * (new Date().getTime() - new Date(2012, 0, 1).getTime()));
      const day = date.getDay();
      const month = date.getMonth();
      const year = date.getFullYear();
      this.createdAt[i] = (day+'/'+month+'/'+year); 
    };

  }
  files: File[] = [];

  onSelect(event: any) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }
  
  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }
  openCreateDatasetModal(content: any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm creation'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }
}

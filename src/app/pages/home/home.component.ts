import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { NgbCarouselConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinner, NgxSpinnerService } from 'ngx-spinner';
import { routes } from 'src/app/app-routing.module';
import { HttpService } from 'src/app/services/http.service';
import { KeycloakUtilsService } from 'src/app/services/keycloak-utils.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit {
  
  points_month = 80;
  points_week = 35;
  points_trans = $localize`Points`
  sweepstake_trans = $localize`sweepstake`
  caroussel_conf = {
    loop: true,
    autoplay: true,
    dots: true,
    width: 610,
    height: 500,
    cellWidth: 300
  }

  slideConfig = {"slidesToShow": 2, "slidesToScroll": 1, "arrows": true};


  tasks: any = [];
  view_task_history: string = $localize`View task history`;
  task_dictionary: any = {
    "plugin-installation": $localize`Download the plugin`,
    "ad-seen": $localize`Display 3 or more ads`,
    "upload-personal-details-data": $localize`Update your personal data`,
    "upload-browsing-data": $localize`Upload browsing data`,
    "upload-location-data": $localize`Upload location history`,
    "upload-social-data": $localize`Upload social data`,
    "upload-financial-data": $localize`Upload financial data`,
    "participating-in-marketplace": $localize`Participating in marketplace`,
    "invite-friends": $localize`Invite friends`,
    "set-consents": $localize`Set consents`,
  };

  task_type_dictionary: any = {
    'task': $localize`Task`,
    'dataOffer': $localize`Data offer`
  };

  data_offers_dictionary: any = {
    "personal-information": $localize`Personal information`

  };

  sweepstake_description_dictionary: any = {
    'Big sweepstake': $localize`Big sweepstake`,
    'Weekly sweepstake': $localize`Weekly sweepstake`,
    'Monthly sweepstake': $localize`Monthly sweepstake`
  };

  periodicity_dictionary: any = {
    "one-time": $localize`One time`,
    "daily": $localize`Daily`,
    "weekly": $localize`Weekly`,
    "monthly": $localize`Monthly`
  };



  pending_tasks: any = [];
  completed_tasks: any = [];
  extra_tasks: any = [];
  transaction_history: any = [];
  next_sweepstakes: any = [];

  points = 0;
  dataLoaded: boolean = false;

  constructor(config: NgbCarouselConfig, private cdRef: ChangeDetectorRef, private spinner: NgxSpinnerService, private http: HttpService,  
    private router: Router, private kc_utils: KeycloakUtilsService, private modalService: NgbModal) {
      config.showNavigationArrows = true;
      config.showNavigationIndicators = true;
     }

  ngOnInit(): void {
    this.spinner.show();
    this.http.tmGetTasks().subscribe(data => {
      this.tasks = data;
      this.splitTasks();

      console.log('this.completed_tasks', this.completed_tasks)
      console.log('this.pending_tasks', this.pending_tasks)
      console.log('this.extra_tasks', this.extra_tasks)

      this.http.tmGetPoints().subscribe(data => {
        console.log('points:', data)
        this.points = data.allTime;
        this.points_month = data.lastMonth;
        this.points_week = data.lastWeek;

        this.http.tmGetSweepstakes().subscribe(data => {
          console.log('sweepstakes:', data);
          this.next_sweepstakes = data;
          this.dataLoaded = true;
          this.spinner.hide();
          this.cdRef.detectChanges();
        })

      })

    });
    this.kc_utils.checkToken();
  }

  splitTasks(){
    for(let i = 0; i<this.tasks.length; i++){
      if(this.tasks[i].completed && this.tasks[i].id !== 'ad-seen')              
          this.completed_tasks.push(this.tasks[i]);
        
      else{
        if(this.tasks[i].id === 'invite-friends' ||
          this.tasks[i].id === 'upload-location-data' ||
          this.tasks[i].id === 'participating-in-marketplace'){
            this.extra_tasks.push(this.tasks[i]);
          }
          else{
          this.pending_tasks.push(this.tasks[i]);
        }
      }
    }
  }

  openBrowseWebModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {
      if(result === 'upload data'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openPersonalDataModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
      if(result === 'Go'){
        this.router.navigateByUrl('/my-data');
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openSetConsentsModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
      if(result === 'Go'){
        this.router.navigateByUrl('/my-consents');
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDownloadPluginModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
      if(result === 'Go'){
        window.location.href = 'https://chrome.google.com/webstore/detail/easypims-focus-groups/mjhbfkhkbhmlpdjmeifadkinkcemmlfa?hl=es&authuser=0';
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDisplayAdsModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
      if(result === 'Go'){
        this.router.navigateByUrl('/show-ads');
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openInviteFriendsModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
      if(result === 'Go'){
        this.router.navigateByUrl('/settings');
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openLocationHistoryModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
      if(result === 'Go'){
        this.router.navigateByUrl('/my-data');
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openParticipateMarketplaceModal(content: any) { 
      this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       
        if(result === 'Go'){
        }
      }, (reason) => {
          // If dismissed 
      });
  }

  openViewCompletedTasksModal(content: any) { 
    this.spinner.show();
    this.http.tmGetTransactions().subscribe(data => {
      console.log(data)
      this.spinner.hide();
      this.transaction_history = data.reverse();

      this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {       

        if(result === 'Go'){
        }
      }, (reason) => {
          // If dismissed 
      });
    })
  }
}

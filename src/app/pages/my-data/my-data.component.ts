import { ChangeDetectorRef, Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgModel, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { KeycloakUtilsService } from 'src/app/services/keycloak-utils.service';
import { concat, forkJoin, from, Observable, of, Subscription } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import * as moment from 'moment';
import { ngfModule, ngf } from "angular-file"
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpEvent, HttpRequest, HttpResponse } from '@angular/common/http';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-my-data',
  templateUrl: './my-data.component.html',
  styleUrls: ['./my-data.component.scss']
})
export class MyDataComponent implements OnInit {


  constructor(private kc_utils: KeycloakUtilsService, private http: HttpService, private cdRef: ChangeDetectorRef,
    private spinner: NgxSpinnerService, private modalService: NgbModal) {}
    


  httpEvent:Event
  uploadedFiles: boolean = false;
  screen_width = window.innerWidth;
  screen_height = window.innerHeight;
  maxSize = 1000000;
  uploadPercent = 0;
  file:File[] = [];
  lastInvalids:any;
  view: string = $localize`Details`;
  update_data: string = $localize`Update data`;
  select_file_trans: string = $localize`Select file`;
  upload_trans: string = $localize`Upload`;
  loc_hist_trans: string = $localize`Watch the video and follow the instructions to export location history data from Google.`;
  dataAvailable: boolean = false;
  graphData: any;
  uploadedBH: boolean = false;
  accept = '.zip';
  uploadedLH: boolean = false;
  filledForm = new FormGroup({
    'first-name': new FormControl(),
    'last-name': new FormControl(),
    'birth-day': new FormControl(),
    'birth-month': new FormControl(),
    'birth-year': new FormControl(),
    'birth-date': new FormControl(),
    'country': new FormControl(),
    'gender': new FormControl(),
    'address': new FormControl(),
    'telephone': new FormControl(),
    'household-members': new FormControl(),
    'job': new FormControl(),
    'income': new FormControl(),
    'education': new FormControl()
  });

  uploadProgress: any = '';
  uploadProgressnum: number = 0;
  oldForm: any;

  selectedDate: NgModel;
  visited_cities: number = 0;
  browsed_websites: number = 0;
  receivedData: any;

  loadedData: boolean = false;

  // FORM LISTS
  monthList =[
    '',
    $localize`January`, 
    $localize`February`, 
    $localize`March`, 
    $localize`April`, 
    $localize`May`, 
    $localize`June`, 
    $localize`July`, 
    $localize`August`, 
    $localize`September`, 
    $localize`October`, 
    $localize`November`, 
    $localize`December`
  ];
  dayList: string[] = ['']
  yearList: string[] = ['']
  jobList: string[] = [
    '',
    $localize`Architecture and Engineering`,	
    $localize`Arts, Design, Entertainment, Sports, and Media`,	
    $localize`Building and Grounds Cleaning and Maintenance`,	
    $localize`Business and Financial Operations`,	
    $localize`Community and Social Services`,	
    $localize`Computer and Mathematical`,	
    $localize`Construction and Extraction`,	
    $localize`Education, Training, and Library`,	
    $localize`Farming, Fishing, and Forestry`,	
    $localize`Food Preparation and Serving Related`,	
    $localize`Healthcare Practitioners and Technical`,	
    $localize`Healthcare Support`,	
    $localize`Installation, Maintenance, and Repair`,	
    $localize`Legal and related`,	
    $localize`Life, Physical, and Social Science`,	
    $localize`Management`,	
    $localize`Military`,	
    $localize`Office and Administrative Support`,	
    $localize`Personal Care and Service`,	
    $localize`Production`,	
    $localize`Protective Service`,	
    $localize`Sales, Marketing and related`,	
    $localize`Student`,	
    $localize`Unemployed`                   
  ];

  genderList: string[] = [
    '',
    $localize`Male`,
    $localize`Female`,
    $localize`Other`
  ];

  educationList: string[] = [
    '',
    $localize`No formal education`,     
    $localize`Primary education`,       
    $localize`Secondary education`,     
    $localize`Vocational qualification`,
    $localize`Bachelor's degree`,       
    $localize`Master's degree`,         
    $localize`Doctorate or higher`,     
  ];
  incomeList: any[] = [
    ['',''],
    [$localize`Less than 10K € net per year`, '10000'],
    [$localize`10K - 20K € net per year`, '15000'],
    [$localize`20K - 30K € net per year`, '25000'],
    [$localize`30K - 40K € net per year`, '35000'],
    [$localize`40K - 50K € net per year`, '45000'],
    [$localize`50K - 60K € net per year`, '55000'],
    [$localize`More than 60k € net per year`, '65000'],
  ];
  selectedFile: any;
  pluginInstalled: boolean = false;
  locationHistoryUploaded: boolean = false;


  uploading: boolean = false;
  ngOnInit() {    
    this.initForms();
    this.kc_utils.checkToken();
    this.spinner.show();
    this.http.pdsGetPersonalData('personal-information').subscribe(data => {
      this.receivedData = data;
      console.log('Received data:',data)
      this.fillPersonalDataForm();
      this.loadedData = true;
      this.http.pdsGetGraphData().subscribe(data => {
        this.setUpLabels_BH_LH(data);
        this.dataAvailable = true;
        this.graphData = data;
        console.log(this.graphData)
        this.http.tmGetTasks().subscribe(data => {
          console.log('tasks', data)
          this.checkTasks(data);
          this.spinner.hide();
          this.cdRef.detectChanges();
        });
      });
    });
  }



  setUpLabels_BH_LH(data: any) {
    for(let i = 0; i<data['macro-groups'].length; i++){
      if(data['macro-groups'][i].group_name === 'browsing-history')
        this.browsed_websites = data['macro-groups'][i].total
      if(data['macro-groups'][i].group_name === 'location-history')
        this.visited_cities = data['macro-groups'][i].total
    }
  }

  checkTasks(data: any){
    for(let i = 0; i<data.length; i++){
      if(data[i].id === 'upload-location-data'){
        if(data[i].completed){
          this.locationHistoryUploaded = true;
        }
      }
      if(data[i].id === 'plugin-installation'){
        if(data[i].completed){
          this.pluginInstalled = true;
        }
      }
    }
  }

  onFileSelected(event: any){
    console.log('event', event)
    this.selectedFile = event.target.files[0];
    this.cdRef.detectChanges();
  }

  onUpload(event: any){
    const opts = {
      reportProgess: true,
      observe: ''
    }
    this.uploading = true;

    this.http.pdsPostLocationHistory(this.selectedFile).subscribe(event =>{

      console.log(event)

      this.uploadProgressnum = event.loaded * 100 / event.total
      this.uploadProgress = this.uploadProgressnum.toString()+'%';
      this.cdRef.detectChanges();

      if(event.total / event.loaded === 1){
        this.uploading = false;
        this.uploadedFiles = true;
        this.cdRef.detectChanges();
      }

    })
  }



  async onSubmit(){
    this.spinner.show()
    console.log('Form untouched:', this.filledForm.value)

    // GET NON EMPTY FIELDS FROM THE FORM
    Object.keys(this.filledForm.value).forEach(key => {
      console.log('new_form', this.filledForm.value[key])
      if (this.filledForm.value[key] === null || this.filledForm.value[key].length === 0) 
        delete this.filledForm.value[key];
    });
    
    // GET IDS OF ALL THE RECEIVED PERSONAL DATA
    // DELETE ALL
    let obs_array: Observable<any>[] = [];
    if(this.receivedData.data.length !== 0) {
      for(let i = 0; i<this.receivedData.data.length; i++){
        obs_array.push(this.http.pdsDeletePersonalData(this.receivedData.data[i].id));
      }
      await new Promise<void>((resolve, reject) => {
        forkJoin(obs_array).subscribe(() => resolve(), (err) => reject(err));
      })
    }


    // DATE FORM MANAGEMENT
    if(
    this.filledForm.value['birth-day'] && 
    this.filledForm.value['birth-month'] && 
    this.filledForm.value['birth-year'] ){
      
      this.filledForm.value['birth-date'] = this.filledForm.value['birth-year']+'-'+
      moment().month(this.filledForm.value['birth-month']).format("MM")+'-'+moment().date(this.filledForm.value['birth-day']).format('DD')+ ' 00:00:00';
      delete this.filledForm.value['birth-year'];
      delete this.filledForm.value['birth-day'];
      delete this.filledForm.value['birth-month'];
    }

    
    console.log('After having deleted the birth dates', this.filledForm.value)
    console.log('Birth date', this.filledForm.value['birth-date'])
    // SEND ALL THE DATA THAT IS IN THE FORM
    this.http.pdsPostPersonalData(this.filledForm.value).subscribe(data => {
      this.spinner.hide();
      })
  }


  fillPersonalDataForm(){
    for (let i = 0; i < this.receivedData['data'].length; i++) {
      if (this.receivedData['data'][i].metadata == 'birth-date') {
        let date = this.receivedData['data'][i].value['birth-date'];
        this.filledForm.patchValue({
          'birth-day': (date.split(' ')[0].split('-')[2]).replace('0',''),
          'birth-month': this.monthList[parseInt(date.split(' ')[0].split('-')[1])],
          'birth-year': date.split(' ')[0].split('-')[0]
        });
      }
      else if (this.receivedData['data'][i].metadata == 'first-name') {
        this.filledForm.patchValue({
          'first-name': this.receivedData['data'][i].value['first-name']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'last-name') {
        this.filledForm.patchValue({
          'last-name': this.receivedData['data'][i].value['last-name']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'income') {
        this.filledForm.patchValue({
          'income': this.receivedData['data'][i].value['income']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'country') {
        this.filledForm.patchValue({
          'country': this.receivedData['data'][i].value['country']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'gender') {
        this.filledForm.patchValue({
          'gender': this.receivedData['data'][i].value['gender']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'address') {
        this.filledForm.patchValue({
          'address': this.receivedData['data'][i].value['address']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'telephone') {
        this.filledForm.patchValue({
          'telephone': this.receivedData['data'][i].value['telephone']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'household-members') {
        this.filledForm.patchValue({
          'household-members': this.receivedData['data'][i].value['household-members']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'job') {
        this.filledForm.patchValue({
          'job': this.receivedData['data'][i].value['job']
        });
      }
      else if (this.receivedData['data'][i].metadata == 'education') {
        this.filledForm.patchValue({
          'education': this.receivedData['data'][i].value['education']
        });
      }
    }
    this.oldForm = this.filledForm.value;
  }

  openLocationHistoryModal(content: any) { 
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {
      if(result === 'upload data'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }
  
  initForms(){
        // Fill year form list
        for(let i = 0; i< 120; i++){
          this.yearList.push((1900 + i).toString());
        }

        // Fill day form list
        for(let i = 0; i< 31; i++){
          this.dayList.push((1 + i).toString());
        }
  } 
}


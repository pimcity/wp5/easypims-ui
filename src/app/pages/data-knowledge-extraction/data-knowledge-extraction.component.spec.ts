import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataKnowledgeExtractionComponent } from './data-knowledge-extraction.component';

describe('DataKnowledgeExtractionComponent', () => {
  let component: DataKnowledgeExtractionComponent;
  let fixture: ComponentFixture<DataKnowledgeExtractionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataKnowledgeExtractionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataKnowledgeExtractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

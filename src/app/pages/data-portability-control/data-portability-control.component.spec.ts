import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPortabilityControlComponent } from './data-portability-control.component';

describe('DataPortabilityControlComponent', () => {
  let component: DataPortabilityControlComponent;
  let fixture: ComponentFixture<DataPortabilityControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataPortabilityControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataPortabilityControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

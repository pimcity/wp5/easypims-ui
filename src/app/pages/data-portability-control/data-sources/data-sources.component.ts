import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y/input-modality/input-modality-detector';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpService } from 'src/app/services/http.service';
import { DataSource, DataSourcePost } from '../../../models/data-portability-control'

@Component({
  selector: 'app-data-sources',
  templateUrl: './data-sources.component.html',
  styleUrls: ['./data-sources.component.scss']
})
export class DataSourcesComponent implements OnInit {

  constructor(private modalService: NgbModal, private http: HttpService,
    private cd: ChangeDetectorRef, private fb: FormBuilder, private spinner: NgxSpinnerService) { }

  selectedDataset: DataSource;
  receivedData: DataSource[] = []
  loadedData: boolean = false;
  createDatasourceForm: FormGroup;

  ngOnInit(): void {

    // this.http.dpcGetDatasources().subscribe(data => {
    //   console.log(data)
    //   this.receivedData = data;
    //   this.loadedData = true;
    //   this.cd.detectChanges();
    // });

  }

  openCreateDatasetModal(content: any){
    this.createDatasourceForm = this.fb.group({
      name: new FormControl('', [
        Validators.required
      ]),
      type: new FormControl('', [
        Validators.required
      ]),
      class: new FormControl('', [
        Validators.required
      ]),
      version: new FormControl('', [
        Validators.required
      ])
    });
  
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm creation'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDeleteModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        this.spinner.show();
        // this.http.dpcDeleteDatasource(dataset._id).subscribe(data => {
        //   this.spinner.hide();
        //   window.location.reload();
        // })
        console.log('content', content)
        console.log('dataset', dataset)
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }


  openEditModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm edit'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDeactivateModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm deactivate'){
        // this.http.dpcPatchDatasource(dataset._id, false).subscribe(data => {
        //   window.location.reload();
        //   console.log('data:', data)
        // })
      }
    }, (reason) => {
        // If dismissed 
    });
  }
  
  openActivateModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm activate'){
        // this.http.dpcPatchDatasource(dataset._id, true).subscribe(data => {
        //   window.location.reload();
        //   console.log('data:', data)
        // })
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  createDatasource(){
    this.spinner.show()
    const new_datasource: DataSourcePost = {
      "class": this.createDatasourceForm.get('class')!.value,
      "manifest-version": this.createDatasourceForm.get('version')!.value,
      "name": this.createDatasourceForm.get('name')!.value,
      "type": this.createDatasourceForm.get('type')!.value
    }
    console.log(new_datasource)

    // this.http.dpcPostDatasource(new_datasource).subscribe(data => {
    //   this.spinner.hide();
    //   console.log('data', data);
    // });
  }


}

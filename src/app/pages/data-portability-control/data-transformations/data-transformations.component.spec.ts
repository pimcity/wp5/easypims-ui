import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTransformationsComponent } from './data-transformations.component';

describe('DataTransformationsComponent', () => {
  let component: DataTransformationsComponent;
  let fixture: ComponentFixture<DataTransformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataTransformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTransformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

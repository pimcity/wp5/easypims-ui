import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/services/http.service';
import { DataTransformation } from '../../../models/data-portability-control'

@Component({
  selector: 'app-data-transformations',
  templateUrl: './data-transformations.component.html',
  styleUrls: ['./data-transformations.component.scss']
})
export class DataTransformationsComponent implements OnInit {

  constructor(private modalService: NgbModal, private http: HttpService,
    private cd: ChangeDetectorRef) { }

  selectedDataset: DataTransformation;
  data: DataTransformation[] = []
  loadedData: boolean = false;

  ngOnInit(): void {

  }

  openCreateDatasetModal(content: any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm creation'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDeleteModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }


  openEditModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm edit'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDeactivateModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm deactivate'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openActivateModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm activate'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }
}

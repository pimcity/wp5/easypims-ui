import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/services/http.service';
import { DataExport } from '../../../models/data-portability-control'
@Component({
  selector: 'app-data-exports',
  templateUrl: './data-exports.component.html',
  styleUrls: ['./data-exports.component.scss']
})
export class DataExportsComponent implements OnInit {

  constructor(private modalService: NgbModal, private http: HttpService,
    private cd: ChangeDetectorRef) { }

  selectedDataset: DataExport;
  data: DataExport[] = []
  loadedData: boolean = false;
    
  ngOnInit(): void {
    // this.http.dpcGetDataExports().subscribe(data => {
    //   console.log(data)
    //   this.data = data;
    //   this.loadedData = true;
    //   this.cd.detectChanges();
    // });
  }

  openCreateDatasetModal(content: any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm creation'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDeleteModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }


  openEditModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    console.log(this.selectedDataset)
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm edit'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openDeactivateModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm deactivate'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openActivateModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm activate'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }


  editData(){

  }

  deactivateData(){

  }

  deleteData(){

  }
}

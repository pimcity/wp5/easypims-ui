import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-data-portability-control',
  templateUrl: './data-portability-control.component.html',
  styleUrls: ['./data-portability-control.component.scss']
})
export class DataPortabilityControlComponent implements OnInit {

  constructor(private http: HttpService) { }

  uploaded_facebook: boolean = false;
  uploaded_truelayer: boolean = true;

  ngOnInit(): void {
    // this.http.dpcPostRequestAuthToken().subscribe(data => {
    //   localStorage.setItem('dpcAuthToken', data.access_token);
    // })
  }

}

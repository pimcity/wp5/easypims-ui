import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpChartsComponent } from './up-charts.component';

describe('UpChartsComponent', () => {
  let component: UpChartsComponent;
  let fixture: ComponentFixture<UpChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

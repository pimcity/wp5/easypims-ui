import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataValuationToolsComponent } from './data-valuation-tools.component';

describe('DataValuationToolsComponent', () => {
  let component: DataValuationToolsComponent;
  let fixture: ComponentFixture<DataValuationToolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataValuationToolsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataValuationToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

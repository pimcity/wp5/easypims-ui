import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { PersonalDataBrowsingHistory, PersonalDataLocationHistory } from 'src/app/models/personal-data-safe';
import { HttpService } from 'src/app/services/http.service';
import { BrowsingHistoryComponent } from '../browsing-history/browsing-history.component';
import * as JSZip from 'jszip';

@Injectable({
  providedIn: 'root'
})
export class ExtractDataService {

  constructor(private http: HttpService, private sanitizer: DomSanitizer) { }

  generateDownloadUri(data: any){
    const theJSON = JSON.stringify(data, null, "\t");
    const uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    return uri;
  }

  extractAllData(){
    const jszip = new JSZip();
    var subject = new Subject<SafeUrl>();

    // RETRIEVE ALL THE DATA
    this.http.pdsGetPersonalData('personal-information').subscribe(personal_information => {
      this.http.pdsGetPersonalData('browsing-history').subscribe(browsing_history => {
        this.http.pdsGetPersonalData('location-history').subscribe(location_history => {
          this.http.pdsGetMacroGroups().subscribe(macro_groups => {
            // GENERATE ALL THE FILES
            jszip.file('personal-information.json', JSON.stringify(personal_information, null, '\t'));
            jszip.file('browsing-history.json', JSON.stringify(browsing_history, null, '\t'));
            jszip.file('location-history.json', JSON.stringify(location_history, null, '\t'));
            jszip.file('schema.json', JSON.stringify(macro_groups, null, '\t'));
            // GENERATE ZIP AND SAFE URI
            jszip.generateAsync({ type: 'blob' }).then( content => {
              // const uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(content));
              // subject.next(uri);
            });
          });
        });
      });
    });
  }

  extractPersonalData(type: string): Observable<SafeUrl>{
    var subject = new Subject<SafeUrl>();
    this.http.pdsGetPersonalData(type).subscribe(data => {
      subject.next(this.generateDownloadUri(data))
    });
    return subject.asObservable();
  }
  
}

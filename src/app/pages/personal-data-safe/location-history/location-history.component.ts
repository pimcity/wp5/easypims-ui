import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { latLng, marker, tileLayer } from 'leaflet';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpService } from 'src/app/services/http.service';
import { PersonalDataLocationHistory, DataLocationHistory } from '../../../models/personal-data-safe'

@Component({
  selector: 'app-location-history',
  templateUrl: './location-history.component.html',
  styleUrls: ['./location-history.component.scss']
})
export class LocationHistoryComponent implements OnInit {

  
  constructor(private modalService: NgbModal, private http: HttpService,
    private cd: ChangeDetectorRef, private spinner: NgxSpinnerService) {   }

  selectedDataset: DataLocationHistory;
  loadedData: boolean = false;
  receivedData: PersonalDataLocationHistory;
  
  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    ],
    zoom: 5,
    center: latLng(46.879966, -121.726909)
  };

  ngOnInit(): void {
    this.spinner.show()
    this.http.pdsGetPersonalData('location-history').subscribe(data => {
      this.receivedData = data;
      this.loadedData = true;
      this.spinner.hide()
      this.cd.detectChanges();
    });
  }

  layers = [
    marker([ 46.879966, -121.726909 ])
  ];

  openDeleteModal(content: any, dataset: any) {
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  openMapModal(content: any, row: any) {
    this.selectedDataset = row;
    this.options = {
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
      ],
      zoom: 5,
      center: latLng(this.selectedDataset.value['visited-location'].latitude,
      this.selectedDataset.value['visited-location'].longitude)
    };
    this.layers = [
      marker([ this.selectedDataset.value['visited-location'].latitude,
      this.selectedDataset.value['visited-location'].longitude ])
    ]
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }


  openAllMapModal(content: any) {

    // ARRAYS TO SUM THE AVERAGE OF COORDS TO CENTER THE MAP THERE
    let lats = [];
    let longs = [];

    for(let i = 0; i<this.receivedData.data.length; i++){
      lats.push(this.receivedData.data[i].value['visited-location'].latitude);
      longs.push(this.receivedData.data[i].value['visited-location'].longitude);

      // ADD THE MARKERS INTO THE MAP
      this.layers.push(marker([
      this.receivedData.data[i].value['visited-location'].latitude,
      this.receivedData.data[i].value['visited-location'].longitude
      ]))
    }

    const lat_avg = (lats.reduce((a, b) => a + b, 0) / lats.length) || 0;
    const long_avg = (longs.reduce((a, b) => a + b, 0) / longs.length) || 0;

    this.options = {
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
      ],
      zoom: 5,
      center: latLng(lat_avg, long_avg)
    };
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalDataSafeComponent } from './personal-data-safe.component';

describe('PersonalDataSafeComponent', () => {
  let component: PersonalDataSafeComponent;
  let fixture: ComponentFixture<PersonalDataSafeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalDataSafeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDataSafeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgModel } from '@angular/forms';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { from, Observable, Observer } from 'rxjs';


@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {

  constructor(private http: HttpService, private cdRef: ChangeDetectorRef,
    private spinner: NgxSpinnerService) {}

  filledForm = new FormGroup({
    firstname: new FormControl(),
    lastname: new FormControl(),
    birth_date: new FormControl(),
    age: new FormControl(),
  });

  selectedDate: NgModel;
  
  receivedData: any;

  loadedData: boolean = false;

  ngOnInit() {    
    this.spinner.show();

    this.http.pdsGetPersonalData('personal-information').subscribe(data => {
      this.receivedData = data;
      console.log(data)
      for (let i = 0; i < this.receivedData['data'].length; i++) {
        if (this.receivedData['data'][i].metadata == 'birth-data') {
          this.filledForm.patchValue({
            birth_date: this.receivedData['data'][i].value['birth-data']
          });
        }
        else if (this.receivedData['data'][i].metadata == 'first name') {

          this.filledForm.patchValue({
            firstname: this.receivedData['data'][i].value['first-name']['first-name']
          });
        }
        else if (this.receivedData['data'][i].metadata == 'last-name') {
          this.filledForm.patchValue({
            lastname: this.receivedData['data'][i].value['last-name']
          });
        }
        else if (this.receivedData['data'][i].metadata == 'age') {
          this.filledForm.patchValue({
            age: this.receivedData['data'][i].value['age']
          });
        }
      }
      this.loadedData = true;
      this.spinner.hide()
      this.cdRef.detectChanges();
    });
  }

  onSubmit(){
    console.log(this.filledForm.value)
  }
}

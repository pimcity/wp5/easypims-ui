import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { HttpService } from '../../../services/http.service'
import { GraphData, TokenObject } from 'src/app/models/personal-data-safe'; 
import { NgxSpinnerService } from 'ngx-spinner';
import { ExtractDataService } from '../services/extract-data.service';
import { SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-pds-home',
  templateUrl: './pds-home.component.html',
  styleUrls: ['./pds-home.component.scss']
})
export class PdsHomeComponent implements OnInit {

  constructor(private http: HttpService, private cdRef:ChangeDetectorRef, private spinner: NgxSpinnerService,
    private data_service: ExtractDataService) { }

  tokenObject: TokenObject; 
  graphData: GraphData;

  downloadPersonalInfoHref: SafeUrl;
  downloadBrowsingHistoryHref: SafeUrl;
  downloadLocationHistoryHref: SafeUrl;

  dataAvailable: boolean = false;
  ngOnInit(): void {
    // this.spinner.show();

    // GET/STORE ACCESS AND REFRESH TOKENS
    this.http.pdsPostToken('lucas_martinez', 'test_password').subscribe(data => {
      this.tokenObject = data;
    console.log(data)

      localStorage.setItem('pdsAccessToken', this.tokenObject.access)
      localStorage.setItem('pdsRefreshToken', this.tokenObject.refresh)

      this.http.pdsGetGraphData().subscribe(data => {
        this.dataAvailable = true;
        this.graphData = data;
        this.initDownloadLinks()
      })
    });
  }

  initDownloadLinks(){
    // SETUP EXTRACT BUTTONS
    this.data_service.extractPersonalData('personal-information').subscribe(url =>{
      this.downloadPersonalInfoHref = url;
      this.data_service.extractPersonalData('browsing-history').subscribe(url => {
        this.downloadBrowsingHistoryHref = url;
        this.data_service.extractPersonalData('location-history').subscribe(url => {
          this.downloadLocationHistoryHref = url;
          this.cdRef.detectChanges();
          this.spinner.hide();
        })
      });
      }
    );
  }
}

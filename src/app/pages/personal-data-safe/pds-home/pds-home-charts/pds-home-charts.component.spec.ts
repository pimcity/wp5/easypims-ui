import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdsHomeChartsComponent } from './pds-home-charts.component';

describe('PdsHomeChartsComponent', () => {
  let component: PdsHomeChartsComponent;
  let fixture: ComponentFixture<PdsHomeChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdsHomeChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdsHomeChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { getCSSVariableValue } from 'src/app/_metronic/kt/_utils';
import { GraphData } from 'src/app/models/personal-data-safe';

@Component({
  selector: 'app-pds-home-charts',
  templateUrl: './pds-home-charts.component.html',
  styleUrls: ['./pds-home-charts.component.scss']
})
export class PdsHomeChartsComponent implements OnInit {

  constructor() { }

  @Input()
  receivedGraphData: GraphData;


  // Line chart data
  lineChartData: number[] = [];
  lineChartAxis: string[] = [];
  lineChartOptions: any = {};
  x_axis : string[] = [];

  // Donut and bar charts data 
  donutBarData: number[] = [];
  barChartOptions: any = {};
  doughnutChartOptions: any = {};

  async ngOnInit(): Promise<void> {

      // Store API data into local attributes to set up graph data
      this.parseReceivedObject();
      this.initCharts();
  }

  initCharts(){
    this.barChartOptions = this.getBarChartOptions(350);
    this.doughnutChartOptions = this.getDoughnutChartOptions(350);
    this.lineChartOptions = this.getLineChartOptions(350);
  }

  parseReceivedObject() {

    // Line chart setup
    for(let i = 0; i < this.receivedGraphData['data-days'].length; i++){
      this.lineChartAxis.push(this.receivedGraphData['data-days'][i].day.split('T')[0]);
      this.lineChartData.push(this.receivedGraphData['data-days'][i].total);
    }


    // Bar and Donut chart setup
    // This is messy, in order to coordinate the indexes of the labels and the values 
    // this is unavoidable due to the Apexchart options limitations, it reads an array instead of an object.
     
    for(let i = 0; i<this.receivedGraphData['macro-groups'].length; i++){
      if(this.receivedGraphData['macro-groups'][i]['group_name'] === 'browsing-history'){
        this.x_axis.push($localize`Browsing History`);
        this.donutBarData.splice(this.x_axis.indexOf('Browsing History'), 0, this.receivedGraphData['macro-groups'][i]['total']);        
      }
      else if(this.receivedGraphData['macro-groups'][i]['group_name'] === 'location-history'){
        this.x_axis.push($localize`Location History`);
        this.donutBarData.splice(this.x_axis.indexOf('Location History'), 0, this.receivedGraphData['macro-groups'][i]['total']);        

      }
      else if(this.receivedGraphData['macro-groups'][i]['group_name'] === 'personal-information'){
        this.x_axis.push($localize`Personal Information`);
        this.donutBarData.splice(this.x_axis.indexOf('Personal Information'), 0, this.receivedGraphData['macro-groups'][i]['total']);        
      }
    }
  }

  getBarChartOptions(height: number) {
    const labelColor = getCSSVariableValue('--bs-gray-500');
    const borderColor = getCSSVariableValue('--bs-gray-200');
  
    return {
      series: [
        {
          name: 'Data count',
          data: this.donutBarData,
        },
      ],
      chart: {
        fontFamily: 'inherit',
        type: 'bar',
        height: height,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '30%',
          borderRadius: 5,
          distributed: true,
        },
      },
      legend: {
        show: false,
      },
      dataLabels: {
        enabled: false,
      },
      xaxis: {
        categories: this.x_axis,
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          style: {
            colors: labelColor,
            fontSize: '12px',
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            colors: labelColor,
            fontSize: '12px',
          },
        },
      },
      fill: {
        opacity: 1,
      },
      states: {
        normal: {
          filter: {
            type: 'none',
            value: 0,
          },
        },
        hover: {
          filter: {
            type: 'none',
            value: 0,
          },
        },
        active: {
          allowMultipleDataPointsSelection: false,
          filter: {
            type: 'none',
            value: 0,
          },
        },
      },
      tooltip: {
        style: {
          fontSize: '12px',
        },
        y: {
          formatter: function (val: number) {
            return val;
          },
        },
      },
      colors: [
        '#FF4560',
        '#00E396',
        '#FEB019'
      ],
      grid: {
        borderColor: borderColor,
        strokeDashArray: 4,
        yaxis: {
          lines: {
            show: true,
          },
        },
      },
    };
  }
  
  getDoughnutChartOptions(height: number) {
    const labelColor = getCSSVariableValue('--bs-gray-500');
    const borderColor = getCSSVariableValue('--bs-gray-200');
  
    return {
      series: this.donutBarData,
      chart: {
        fontFamily: 'inherit',
        type: 'donut',
        height: height,
        toolbar: {
          show: false,
        },    
      },
      labels: this.x_axis,
      colors: ['#FF4560', '#00E396', '#FEB019'],
      legend: {
        show: true,
        position: 'bottom'
      }
    };
  }
  
  getLineChartOptions(height: number) {
    const labelColor = getCSSVariableValue('--bs-gray-500');
    const borderColor = getCSSVariableValue('--bs-gray-200');
    const baseColor = getCSSVariableValue('--bs-info');
    const lightColor = getCSSVariableValue('--bs-light-info');
  
    return {
      series: [
        {
          name: 'Personal data',
          data: this.lineChartData,
        },
      ],
      chart: {
        fontFamily: 'inherit',
        type: 'area',
        height: 350,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {},
      legend: {
        show: false,
      },
      dataLabels: {
        enabled: false,
      },
      fill: {
        type: 'solid',
        opacity: 1,
      },
      stroke: {
        curve: 'smooth',
        show: true,
        width: 3,
        colors: [baseColor],
      },
      xaxis: {
        categories: this.lineChartAxis,
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          style: {
            colors: labelColor,
            fontSize: '12px',
          },
        },
        crosshairs: {
          position: 'front',
          stroke: {
            color: baseColor,
            width: 1,
            dashArray: 3,
          },
        },
        tooltip: {
          enabled: true,
          formatter: undefined,
          offsetY: 0,
          style: {
            fontSize: '12px',
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            colors: labelColor,
            fontSize: '12px',
          },
        },
      },
      states: {
        normal: {
          filter: {
            type: 'none',
            value: 0,
          },
        },
        hover: {
          filter: {
            type: 'none',
            value: 0,
          },
        },
        active: {
          allowMultipleDataPointsSelection: false,
          filter: {
            type: 'none',
            value: 0,
          },
        },
      },
      tooltip: {
        style: {
          fontSize: '12px',
        },
        y: {
          formatter: function (val: number) {
            return  val;
          },
        },
      },
      colors: [lightColor],
      grid: {
        borderColor: borderColor,
        strokeDashArray: 4,
        yaxis: {
          lines: {
            show: true,
          },
        },
      },
      markers: {
        strokeColors: baseColor,
        strokeWidth: 3,
      },
    };
  }
  
}




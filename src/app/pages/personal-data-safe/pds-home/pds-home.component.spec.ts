import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdsHomeComponent } from './pds-home.component';

describe('PdsHomeComponent', () => {
  let component: PdsHomeComponent;
  let fixture: ComponentFixture<PdsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

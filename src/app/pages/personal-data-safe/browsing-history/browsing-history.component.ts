import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpService } from 'src/app/services/http.service';
import { PersonalDataBrowsingHistory, DataBrowsingHistory } from '../../../models/personal-data-safe'
import { Router } from '@angular/router';
import { Sort } from '../../../utils/sort';
import { TableFilterService } from 'src/app/services/table-filter.service';

const extractDomain = require("extract-domain");

@Component({
  selector: 'app-browsing-history',
  templateUrl: './browsing-history.component.html',
  styleUrls: ['./browsing-history.component.scss']
})
export class BrowsingHistoryComponent implements OnInit {

  constructor(private table_filter: TableFilterService, private router: Router, private modalService: NgbModal, private http: HttpService,
    private cd: ChangeDetectorRef, private spinner: NgxSpinnerService) {  }

  cleanedUrls = new Map<string, string>();
  selectedDataset: DataBrowsingHistory;
  receivedData: any[] = [];
  loadedData: boolean = false;
  page: number = 1;
  pageSize: number = 20;
  collectionSize: number;
  rows: any = [];
  filter_text: any = '';
  trans_1: string = $localize`This operation can not be undone.`;

  
  ngOnInit(): void {
    this.spinner.show();
    this.http.pdsGetPersonalData('browsing-history').subscribe(data => {
      console.log('data', data)
      this.initData(data);

      // CLEAN THE URL TO ACCESS TO THE TRANSPARENCY TAGS CORRECTLY
      for(let i = 0; i< this.receivedData.length; i++){
        console.log(i, this.receivedData[i]['url'])
        console.log('DATA',this.receivedData)

        this.cleanedUrls.set(
          this.receivedData[i]['url'],
          extractDomain(this.receivedData[i]['url'])
          );
      }
      this.collectionSize = this.receivedData.length;
      this.refreshTable();
      this.loadedData = true;
      this.spinner.hide();
      this.cd.detectChanges();
    });
  }

  openDeleteModal(content: any, dataset: any) { 
    this.selectedDataset = dataset;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      if(result === 'confirm delete'){
        this.http.pdsDeletePersonalData(dataset.id).subscribe(data => {
          window.location.reload();
        },
        (error => {
          
        }))
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  initData(data: any){
    for(let i = 0 ; i<data.data.length; i++){
      this.receivedData.push({
        url: data.data[i].value.url,
        time: data.data[i].value.time
      })

    }
    const sort = new Sort();
    this.receivedData.sort(sort.startSort('time', 'asc', 'date'))
  }

  searchTable(){
    this.table_filter.tableSearch('table_search_id','BH_table_id', 'table_header');
  }

  refreshTable(){
    this.rows = this.receivedData.reverse();
    this.rows = this.orderData();

    // Used for the pagination
    if(!this.filter_text){
      this.rows = this.rows.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    }
    this.rows = this.rows.slice(0, (1 - 1) * this.pageSize + this.pageSize);
    this.cd.detectChanges();
  }

  orderData(){
    let text = this.filter_text;
    return this.rows.filter((row: any) => {
      const term = text.toLowerCase();
      return row.time.toLowerCase().includes(term)
          || row.url.toLowerCase().includes(term)
    });
  }


}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PcmPreferencesComponent } from './pcm-preferences.component';

describe('PcmPreferencesComponent', () => {
  let component: PcmPreferencesComponent;
  let fixture: ComponentFixture<PcmPreferencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PcmPreferencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PcmPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

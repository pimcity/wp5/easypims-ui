import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ChangeConsent, Data } from 'src/app/models/personal-consent-manager';
import { HttpService } from 'src/app/services/http.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pcm-preferences',
  templateUrl: './pcm-preferences.component.html',
  styleUrls: ['./pcm-preferences.component.scss']
})
export class PcmPreferencesComponent implements OnInit {

  constructor(private http: HttpService, private spinner: NgxSpinnerService, private cd: ChangeDetectorRef) { }

  receivedDataShared: Data[];
  receivedData: Data[];
  showTables: boolean = false;

  ngOnInit(): void {
    // GET THE DATA
    this.spinner.show();
    this.http.pcmGetConsents().subscribe(data => {
      this.http.pcmGetDataShared().subscribe(data_shared => {
        this.receivedData = data;
        this.receivedDataShared = data_shared;
        console.log(this.receivedData)
        console.log(this.receivedDataShared)
        this.showTables = true;
        this.cd.detectChanges()
        this.spinner.hide();
      });
    })
  }

  checkboxChange($event: any, consentId: string, type: string){
    const consentChange: ChangeConsent = {
      'consentId': consentId,
      'active': $event.target.checked
    }
    
    if(type === 'data_shared'){
      this.http.pcmPutDataShared(consentChange).subscribe({
        next: data => {},
        error: error => {
          console.error('There was an error!', error);
        }
      });
    }

    else if(type === 'data'){
      this.http.pcmPutData(consentChange).subscribe({
        next: data => {},
        error: error => {
          console.error('There was an error!', error);
        }
      });
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PcmDashboardComponent } from './pcm-dashboard.component';

describe('PcmDashboardComponent', () => {
  let component: PcmDashboardComponent;
  let fixture: ComponentFixture<PcmDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PcmDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PcmDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

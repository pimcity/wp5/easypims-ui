import { Component, OnInit } from '@angular/core';
import { Dashboard, Data } from 'src/app/models/personal-consent-manager';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pcm-dashboard',
  templateUrl: './pcm-dashboard.component.html',
  styleUrls: ['./pcm-dashboard.component.scss']
})
export class PcmDashboardComponent implements OnInit {

  constructor() { }

  receivedData: Dashboard = {
    id: 'asd',
    name: 'asd',
    email: 'asd',
    accountCreatedAt: 2,
    offersProcessed: 53,
    missedOffers: 22,
    balance: 154
  };

  ngOnInit(): void {

  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalConsentManagerComponent } from './personal-consent-manager.component';

describe('PersonalConsentManagerComponent', () => {
  let component: PersonalConsentManagerComponent;
  let fixture: ComponentFixture<PersonalConsentManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalConsentManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalConsentManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTradingEngineComponent } from './data-trading-engine.component';

describe('DataTradingEngineComponent', () => {
  let component: DataTradingEngineComponent;
  let fixture: ComponentFixture<DataTradingEngineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataTradingEngineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTradingEngineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

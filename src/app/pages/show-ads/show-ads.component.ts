import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { DOCUMENT } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-show-ads',
  templateUrl: './show-ads.component.html',
  styleUrls: ['./show-ads.component.scss']
})
export class ShowAdsComponent implements OnInit {
  appendedChildren: any[] = [];

  constructor(private http: HttpService, private renderer2: Renderer2,
  @Inject(DOCUMENT) private _document: Document) { }
  
  receivedAd: string;

  ngOnInit(): void {
    this.http.saGetAd().subscribe(data => {
      // "data" is a huge string in HTML form containing only empty <script> tags but with a 'src' attr
      // We get the JS code per every script requesting the code from these 'src'
      this.generateIframesFromScripts(data);
    })
  }

  generateIframesFromScripts(data: string){
    // Get the Srcs from the scripts
    const script_srcs = new DOMParser().parseFromString(data, 'text/html').getElementsByTagName('script');
    console.log(data)

    // Generate an iframe per script, embedding it within
    for(let i = 0; i<script_srcs.length; i++){
      let iframe = this.renderer2.createElement('iframe');
      iframe.setAttribute('class', 'iframe-ad');
      iframe.setAttribute('srcdoc', `
      <html>
      <head></head>
      <body>
        <script src="`+ script_srcs[i].src + `"></script>
      </body>
      </html>
      `);
      
      // Save the iframes to get rid of them afterwards
      this.appendedChildren.push(iframe);
      this.renderer2.appendChild(this._document.body.querySelector('div.iframe-holder'), iframe);      
    }
  }

  ngOnDestroy(){
    // Get rid of the iframes
    for(let i = 0; i<this.appendedChildren.length; i++){
      this.renderer2.removeChild(this._document.body, this.appendedChildren[i])
    }
  }

}

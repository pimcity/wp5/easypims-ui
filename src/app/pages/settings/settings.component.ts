import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgModel } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { KeycloakUtilsService } from 'src/app/services/keycloak-utils.service';
import { KeycloakService } from 'keycloak-angular';
import { DomSanitizer } from '@angular/platform-browser';

import {saveAs} from 'file-saver';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { cssClasses } from 'nouislider';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {



  constructor(private modalService: NgbModal, private sanitizer: DomSanitizer, private keycloak: KeycloakService, private http: HttpService, private cdRef: ChangeDetectorRef,
    private spinner: NgxSpinnerService, private kc_utils: KeycloakUtilsService) {
      if(environment.production){
        this.referralLink = window.location.hostname + '/ref/';

      }
      else{
        this.referralLink = window.location.hostname + '/avatar/ref/';

      }
    }

  referralLink: string;
  selectedDate: NgModel;
  receivedData: any;
  loadedData: boolean = false;
  download_data_trans: string = $localize`Download gathered data from Easypims in JSON format. Only a portion of browsing history will be downloaded.`
  user_id: string;
  btn_val: string = $localize`Copy`;

  ngOnInit() {
    this.decodeJWTtoken(localStorage.getItem('pdaAccessToken')!)
    this.kc_utils.checkToken();
    this.InitReferralLink();
    }

  onCopy(){
    this.btn_val = $localize`Copied!`;
  }

  resetPassword(){
    this.keycloak.login({
      action: "UPDATE_PASSWORD",
    })
  }

  exportDownloadedData() {
    this.http.pdsGetPersonalData('').subscribe(data => {
      this.downloadFile(data)
    });
  }

  downloadFile(data: any) {
    const blob = new Blob([JSON.stringify(data, null, "\t")], { type: 'text/json' });
    saveAs(blob, 'Easypims_data.json');
  }

  openDeleteAccountModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then(async (result) => {
      if(result === 'confirm delete'){
        const mail_body = $localize`This is a notification in order to delete the account. Please, give us some feedback regarding your decision in this mail:`
        const mailText = "mailto:"+environment.EasypimsMail+"?subject=EasyPims account deletion request, ID: "+this.user_id+"&body="+ mail_body; // add the links to body
        window.location.href = mailText;
      }
    }, () => {
        // If dismissed 
    });
  }


  
  decodeJWTtoken(token: string){
    try {
      const jwt: any = jwt_decode(token);
      this.user_id = jwt.sub;
      console.log(this.user_id)
    } catch(Error) {
      return null;
    }
    
  }
  InitReferralLink(){
    this.http.tmGetReferral().subscribe(data =>{
      this.referralLink += data.referralCode;
      this.cdRef.detectChanges();      
    })
  }
}

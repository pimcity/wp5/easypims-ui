import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Keycloak } from 'keycloak-angular/lib/core/services/keycloak.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.component.html',
  styleUrls: ['./referrals.component.scss']
})
export class ReferralsComponent implements OnInit {

  // THIS PAGE REDIRECTS TO HOME, IT DOES NOT HAVE A UI, 
  // IT JUST SENDS A POST TO THE TASK MANGAER REFERRAL API
  ref_id: string;

  constructor(private http: HttpService, private activated_route: ActivatedRoute) { }

  ngOnInit(): void {
    this.activated_route.paramMap.subscribe(params => {
      this.ref_id = params.get('ref_id')!; 
      this.http.tmPostReferral(this.ref_id).subscribe(data => {
        window.location.assign(window.location.origin+'/avatar/home');
      },
      (err) => {
        window.location.assign(window.location.origin+'/avatar/home');
        console.log('Referral code already used.')
      })
    });
  }
}

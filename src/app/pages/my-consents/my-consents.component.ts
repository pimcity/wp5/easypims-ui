import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { KeycloakUtilsService } from 'src/app/services/keycloak-utils.service';
import { NgxSpinnerService } from "ngx-spinner";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Sort } from '../../utils/sort';

@Component({
  selector: 'app-my-consents',
  templateUrl: './my-consents.component.html',
  styleUrls: ['./my-consents.component.scss']
})


export class MyConsentsComponent implements OnInit {

  // Content taxonomy dict
  sections_dict = {
    'IAB1': $localize`Arts & Entertainment`,
    'IAB1-1': $localize`Books & Literature`,
    'IAB1-2': $localize`Celebrity Fan/Gossip`,
    'IAB1-3': $localize`Fine Art`,
    'IAB1-4': $localize`Humor`,
    'IAB1-5': $localize`Movies`,
    'IAB1-6': $localize`Music`,
    'IAB1-7': $localize`Television`,
    'IAB2': $localize`Automotive`,
    'IAB2-1': $localize`Auto Parts`,
    'IAB2-2': $localize`Auto Repair`,
    'IAB2-3': $localize`Buying/Selling Cars`,
    'IAB2-4': $localize`Car Culture`,
    'IAB2-5': $localize`Certified Pre-Owned`,
    'IAB2-6': $localize`Convertible`,
    'IAB2-7': $localize`Coupe`,
    'IAB2-8': $localize`Crossover`,
    'IAB2-9': $localize`Diesel`,
    'IAB2-10': $localize`Electric Vehicle`,
    'IAB2-11': $localize`Hatchback`,
    'IAB2-12': $localize`Hybrid`,
    'IAB2-13': $localize`Luxury`,
    'IAB2-14': $localize`MiniVan`,
    'IAB2-15': $localize`Motorcycles`,
    'IAB2-16': $localize`Off-Road Vehicles`,
    'IAB2-17': $localize`Performance Vehicles`,
    'IAB2-18': $localize`Pickup`,
    'IAB2-19': $localize`Road-Side Assistance`,
    'IAB2-20': $localize`Sedan`,
    'IAB2-21': $localize`Trucks & Accessories`,
    'IAB2-22': $localize`Vintage Cars`,
    'IAB2-23': $localize`Wagon`,
    'IAB3': $localize`Business`,
    'IAB3-1': $localize`Advertising`,
    'IAB3-2': $localize`Agriculture`,
    'IAB3-3': $localize`Biotech/Biomedical`,
    'IAB3-4': $localize`Business Software`,
    'IAB3-5': $localize`Construction`,
    'IAB3-6': $localize`Forestry`,
    'IAB3-7': $localize`Government`,
    'IAB3-8': $localize`Green Solutions`,
    'IAB3-9': $localize`Human Resources`,
    'IAB3-10': $localize`Logistics`,
    'IAB3-11': $localize`Marketing`,
    'IAB3-12': $localize`Metals`,
    'IAB4': $localize`Careers`,
    'IAB4-1': $localize`Career Planning`,
    'IAB4-2': $localize`College`,
    'IAB4-3': $localize`Financial Aid`,
    'IAB4-4': $localize`Job Fairs`,
    'IAB4-5': $localize`Job Search`,
    'IAB4-6': $localize`Resume Writing/Advice`,
    'IAB4-7': $localize`Nursing`,
    'IAB4-8': $localize`Scholarships`,
    'IAB4-9': $localize`Telecommuting`,
    'IAB4-10': $localize`U.S. Military`,
    'IAB4-11': $localize`Career Advice`,
    'IAB5': $localize`Education`,
    'IAB5-1': $localize`Education`,
    'IAB5-2': $localize`Adult Education`,
    'IAB5-3': $localize`Art History`,
    'IAB5-4': $localize`Colledge Administration`,
    'IAB5-5': $localize`College Life`,
    'IAB5-6': $localize`Distance Learning`,
    'IAB5-7': $localize`English as a 2nd Language`,
    'IAB5-8': $localize`Language Learning`,
    'IAB5-9': $localize`Graduate School`,
    'IAB5-10': $localize`Homeschooling`,
    'IAB5-11': $localize`Homework/Study Tips`,
    'IAB5-12': $localize`K-6 Educators`,
    'IAB5-13': $localize`Private School`,
    'IAB5-14': $localize`Special Education`,
    'IAB5-15': $localize`Studying Business`,
    'IAB6': $localize`Family & Parenting`,
    'IAB6-1': $localize`Adoption`,
    'IAB6-2': $localize`Babies & Toddlers`,
    'IAB6-3': $localize`Daycare/Pre School`,
    'IAB6-4': $localize`Family Internet`,
    'IAB6-5': $localize`Parenting - K-6 Kids`,
    'IAB6-6': $localize`Parenting teens`,
    'IAB6-7': $localize`Pregnancy`,
    'IAB6-8': $localize`Special Needs Kids`,
    'IAB6-9': $localize`Eldercare`,
    'IAB7': $localize`Health & Fitness`,
    'IAB7-1': $localize`Exercise`,
    'IAB7-2': $localize`A.D.D.`,
    'IAB7-3': $localize`AIDS/HIV`,
    'IAB7-4': $localize`Allergies`,
    'IAB7-5': $localize`Alternative Medicine`,
    'IAB7-6': $localize`Arthritis`,
    'IAB7-7': $localize`Asthma`,
    'IAB7-8': $localize`Autism/PDD`,
    'IAB7-9': $localize`Bipolar Disorder`,
    'IAB7-10': $localize`Brain Tumor`,
    'IAB7-11': $localize`Cancer`,
    'IAB7-12': $localize`Cholesterol`,
    'IAB7-13': $localize`Chronic Fatigue Syndrome`,
    'IAB7-14': $localize`Chronic Pain`,
    'IAB7-15': $localize`Cold & Flu`,
    'IAB7-16': $localize`Deafness`,
    'IAB7-17': $localize`Dental Care`,
    'IAB7-18': $localize`Depression`,
    'IAB7-19': $localize`Dermatology`,
    'IAB7-20': $localize`Diabetes`,
    'IAB7-21': $localize`Epilepsy`,
    'IAB7-22': $localize`GERD/Acid Reflux`,
    'IAB7-23': $localize`Headaches/Migraines`,
    'IAB7-24': $localize`Heart Disease`,
    'IAB7-25': $localize`Herbs for Health`,
    'IAB7-26': $localize`Holistic Healing`,
    'IAB7-27': $localize`IBS/Crohn's Disease`,
    'IAB7-28': $localize`Incest/Abuse Support`,
    'IAB7-29': $localize`Incontinence`,
    'IAB7-30': $localize`Infertility`,
    'IAB7-31': $localize`Men's Health`,
    'IAB7-32': $localize`Nutrition`,
    'IAB7-33': $localize`Orthopedics`,
    'IAB7-34': $localize`Panic/Anxiety Disorders`,
    'IAB7-35': $localize`Pediatrics`,
    'IAB7-36': $localize`Physical Therapy`,
    'IAB7-37': $localize`Psychology/Psychiatry`,
    'IAB7-38': $localize`Senor Health`,
    'IAB7-39': $localize`Sexuality`,
    'IAB7-40': $localize`Sleep Disorders`,
    'IAB7-41': $localize`Smoking Cessation`,
    'IAB7-42': $localize`Substance Abuse`,
    'IAB7-43': $localize`Thyroid Disease`,
    'IAB7-44': $localize`Weight Loss`,
    'IAB7-45': $localize`Women's Health`,
    'IAB8': $localize`Food & Drink`,
    'IAB8-1': $localize`American Cuisine`,
    'IAB8-2': $localize`Barbecues & Grilling`,
    'IAB8-3': $localize`Cajun/Creole`,
    'IAB8-4': $localize`Chinese Cuisine`,
    'IAB8-5': $localize`Cocktails/Beer`,
    'IAB8-6': $localize`Coffee/Tea`,
    'IAB8-7': $localize`Cuisine-Specific`,
    'IAB8-8': $localize`Desserts & Baking`,
    'IAB8-9': $localize`Dining Out`,
    'IAB8-10': $localize`Food Allergies`,
    'IAB8-11': $localize`French Cuisine`,
    'IAB8-12': $localize`Health/Lowfat Cooking`,
    'IAB8-13': $localize`Italian Cuisine`,
    'IAB8-14': $localize`Japanese Cuisine`,
    'IAB8-15': $localize`Mexican Cuisine`,
    'IAB8-16': $localize`Vegan`,
    'IAB8-17': $localize`Vegetarian`,
    'IAB8-18': $localize`Wine`,
    'IAB9': $localize`Hobbies & Interests`,
    'IAB9-1': $localize`Art/Technology`,
    'IAB9-2': $localize`Arts & Crafts`,
    'IAB9-3': $localize`Beadwork`,
    'IAB9-4': $localize`Birdwatching`,
    'IAB9-5': $localize`Board Games/Puzzles`,
    'IAB9-6': $localize`Candle & Soap Making`,
    'IAB9-7': $localize`Card Games`,
    'IAB9-8': $localize`Chess`,
    'IAB9-9': $localize`Cigars`,
    'IAB9-10': $localize`Collecting`,
    'IAB9-11': $localize`Comic Books`,
    'IAB9-12': $localize`Drawing/Sketching`,
    'IAB9-13': $localize`Freelance Writing`,
    'IAB9-14': $localize`Genealogy`,
    'IAB9-15': $localize`Getting Published`,
    'IAB9-16': $localize`Guitar`,
    'IAB9-17': $localize`Home Recording`,
    'IAB9-18': $localize`Investors & Patents`,
    'IAB9-19': $localize`Jewelry Making`,
    'IAB9-20': $localize`Magic & Illusion`,
    'IAB9-21': $localize`Needlework`,
    'IAB9-22': $localize`Painting`,
    'IAB9-23': $localize`Photography`,
    'IAB9-24': $localize`Radio`,
    'IAB9-25': $localize`Roleplaying Games`,
    'IAB9-26': $localize`Sci-Fi & Fantasy`,
    'IAB9-27': $localize`Scrapbooking`,
    'IAB9-28': $localize`Screenwriting`,
    'IAB9-29': $localize`Stamps & Coins`,
    'IAB9-30': $localize`Video & Computer Games`,
    'IAB9-31': $localize`Woodworking`,
    'IAB10': $localize`Home & Garden`,
    'IAB10-1': $localize`Appliances`,
    'IAB10-2': $localize`Entertaining`,
    'IAB10-3': $localize`Environmental Safety`,
    'IAB10-4': $localize`Gardening`,
    'IAB10-5': $localize`Home Repair`,
    'IAB10-6': $localize`Home Theater`,
    'IAB10-7': $localize`Interior Decorating`,
    'IAB10-8': $localize`Landscaping`,
    'IAB10-9': $localize`Remodeling & Construction`,
    'IAB11': $localize`Law, Gov't & Politics`,
    'IAB11-1': $localize`Immigration`,
    'IAB11-2': $localize`Legal Issues`,
    'IAB11-3': $localize`U.S. Government Resources`,
    'IAB11-4': $localize`Politics`,
    'IAB11-5': $localize`Commentary`,
    'IAB12': $localize`News`,
    'IAB12-1': $localize`International News`,
    'IAB12-2': $localize`National News`,
    'IAB12-3': $localize`Local News`,
    'IAB13': $localize`Personal Finance`,
    'IAB13-1': $localize`Beginning Investing`,
    'IAB13-2': $localize`Credit/Debt & Loans`,
    'IAB13-3': $localize`Financial News`,
    'IAB13-4': $localize`Financial Planning`,
    'IAB13-5': $localize`Hedge Fund`,
    'IAB13-6': $localize`Insurance`,
    'IAB13-7': $localize`Investing`,
    'IAB13-8': $localize`Mutual Funds`,
    'IAB13-9': $localize`Options`,
    'IAB13-10': $localize`Retirement Planning`,
    'IAB13-11': $localize`Stocks`,
    'IAB13-12': $localize`Tax Planning`,
    'IAB14': $localize`Society`,
    'IAB14-1': $localize`Dating`,
    'IAB14-2': $localize`Divorce Support`,
    'IAB14-3': $localize`Gay Life`,
    'IAB14-4': $localize`Marriage`,
    'IAB14-5': $localize`Senior Living`,
    'IAB14-6': $localize`Teens`,
    'IAB14-7': $localize`Weddings`,
    'IAB14-8': $localize`Ethnic Specific`,
    'IAB15': $localize`Science`,
    'IAB15-1': $localize`Astrology`,
    'IAB15-2': $localize`Biology`,
    'IAB15-3': $localize`Chemistry`,
    'IAB15-4': $localize`Geology`,
    'IAB15-5': $localize`Paranormal Phenomena`,
    'IAB15-6': $localize`Physics`,
    'IAB15-7': $localize`Space/Astronomy`,
    'IAB15-8': $localize`Geography`,
    'IAB15-9': $localize`Botany`,
    'IAB15-10': $localize`Weather`,
    'IAB16': $localize`Pets`,
    'IAB16-1': $localize`Aquariums`,
    'IAB16-2': $localize`Birds`,
    'IAB16-3': $localize`Cats`,
    'IAB16-4': $localize`Dogs`,
    'IAB16-5': $localize`Large Animals`,
    'IAB16-6': $localize`Reptiles`,
    'IAB16-7': $localize`Veterinary Medicine`,
    'IAB17': $localize`Sports`,
    'IAB17-1': $localize`Auto Racing`,
    'IAB17-2': $localize`Baseball`,
    'IAB17-3': $localize`Bicycling`,
    'IAB17-4': $localize`Bodybuilding`,
    'IAB17-5': $localize`Boxing`,
    'IAB17-6': $localize`Canoeing/Kayaking`,
    'IAB17-7': $localize`Cheerleading`,
    'IAB17-8': $localize`Climbing`,
    'IAB17-9': $localize`Cricket`,
    'IAB17-10': $localize`Figure Skating`,
    'IAB17-11': $localize`Fly Fishing`,
    'IAB17-12': $localize`Football`,
    'IAB17-13': $localize`Freshwater Fishing`,
    'IAB17-14': $localize`Game & Fish`,
    'IAB17-15': $localize`Golf`,
    'IAB17-16': $localize`Horse Racing`,
    'IAB17-17': $localize`Horses`,
    'IAB17-18': $localize`Hunting/Shooting`,
    'IAB17-19': $localize`Inline Skating`,
    'IAB17-20': $localize`Martial Arts`,
    'IAB17-21': $localize`Mountain Biking`,
    'IAB17-22': $localize`NASCAR Racing`,
    'IAB17-23': $localize`Olympics`,
    'IAB17-24': $localize`Paintball`,
    'IAB17-25': $localize`Power & Motorcycles`,
    'IAB17-26': $localize`Pro Basketball`,
    'IAB17-27': $localize`Pro Ice Hockey`,
    'IAB17-28': $localize`Rodeo`,
    'IAB17-29': $localize`Rugby`,
    'IAB17-30': $localize`Running/Jogging`,
    'IAB17-31': $localize`Sailing`,
    'IAB17-32': $localize`Saltwater Fishing`,
    'IAB17-33': $localize`Scuba Diving`,
    'IAB17-34': $localize`Skateboarding`,
    'IAB17-35': $localize`Skiing`,
    'IAB17-36': $localize`Snowboarding`,
    'IAB17-37': $localize`Surfing/Bodyboarding`,
    'IAB17-38': $localize`Swimming`,
    'IAB17-39': $localize`Table Tennis/Ping-Pong`,
    'IAB17-40': $localize`Tennis`,
    'IAB17-41': $localize`Volleyball`,
    'IAB17-42': $localize`Walking`,
    'IAB17-43': $localize`Waterski/Wakeboard`,
    'IAB17-44': $localize`World Soccer`,
    'IAB18': $localize`Style & Fashion`,
    'IAB18-1': $localize`Beauty`,
    'IAB18-2': $localize`Body Art`,
    'IAB18-3': $localize`Fashion`,
    'IAB18-4': $localize`Jewelry`,
    'IAB18-5': $localize`Clothing`,
    'IAB18-6': $localize`Accessories`,
    'IAB19': $localize`Technology & Computing`,
    'IAB19-1': $localize`3-D Graphics`,
    'IAB19-2': $localize`Animation`,
    'IAB19-3': $localize`Antivirus Software`,
    'IAB19-4': $localize`C/C++`,
    'IAB19-5': $localize`Cameras & Camcorders`,
    'IAB19-6': $localize`Cell Phones`,
    'IAB19-7': $localize`Computer Certification`,
    'IAB19-8': $localize`Computer Networking`,
    'IAB19-9': $localize`Computer Peripherals`,
    'IAB19-10': $localize`Computer Reviews`,
    'IAB19-11': $localize`Data Centers`,
    'IAB19-12': $localize`Databases`,
    'IAB19-13': $localize`Desktop Publishing`,
    'IAB19-14': $localize`Desktop Video`,
    'IAB19-15': $localize`Email`,
    'IAB19-16': $localize`Graphics Software`,
    'IAB19-17': $localize`Home Video/DVD`,
    'IAB19-18': $localize`Internet Technology`,
    'IAB19-19': $localize`Java`,
    'IAB19-20': $localize`JavaScript`,
    'IAB19-21': $localize`Mac Support`,
    'IAB19-22': $localize`MP3/MIDI`,
    'IAB19-23': $localize`Net Conferencing`,
    'IAB19-24': $localize`Net for Beginners`,
    'IAB19-25': $localize`Network Security`,
    'IAB19-26': $localize`Palmtops/PDAs`,
    'IAB19-27': $localize`PC Support`,
    'IAB19-28': $localize`Portable`,
    'IAB19-29': $localize`Entertainment`,
    'IAB19-30': $localize`Shareware/Freeware`,
    'IAB19-31': $localize`Unix`,
    'IAB19-32': $localize`Visual Basic`,
    'IAB19-33': $localize`Web Clip Art`,
    'IAB19-34': $localize`Web Design/HTML`,
    'IAB19-35': $localize`Web Search`,
    'IAB19-36': $localize`Windows`,
    'IAB20': $localize`Travel`,
    'IAB20-1': $localize`Adventure Travel`,
    'IAB20-2': $localize`Africa`,
    'IAB20-3': $localize`Air Travel`,
    'IAB20-4': $localize`Australia & New Zealand`,
    'IAB20-5': $localize`Bed & Breakfasts`,
    'IAB20-6': $localize`Budget Travel`,
    'IAB20-7': $localize`Business Travel`,
    'IAB20-8': $localize`By US Locale`,
    'IAB20-9': $localize`Camping`,
    'IAB20-10': $localize`Canada`,
    'IAB20-11': $localize`Caribbean`,
    'IAB20-12': $localize`Cruises`,
    'IAB20-13': $localize`Eastern Europe`,
    'IAB20-14': $localize`Europe`,
    'IAB20-15': $localize`France`,
    'IAB20-16': $localize`Greece`,
    'IAB20-17': $localize`Honeymoons/Getaways`,
    'IAB20-18': $localize`Hotels`,
    'IAB20-19': $localize`Italy`,
    'IAB20-20': $localize`Japan`,
    'IAB20-21': $localize`Mexico & Central America`,
    'IAB20-22': $localize`National Parks`,
    'IAB20-23': $localize`South America`,
    'IAB20-24': $localize`Spas`,
    'IAB20-25': $localize`Theme Parks`,
    'IAB20-26': $localize`Traveling with Kids`,
    'IAB20-27': $localize`United Kingdom`,
    'IAB21': $localize`Real Estate`,
    'IAB21-1': $localize`Apartments`,
    'IAB21-2': $localize`Architects`,
    'IAB21-3': $localize`Buying/Selling Homes`,
    'IAB22': $localize`Shopping`,
    'IAB22-1': $localize`Contests & Freebies`,
    'IAB22-2': $localize`Couponing`,
    'IAB22-3': $localize`Comparison`,
    'IAB22-4': $localize`Engines`,
    'IAB23': $localize`Religion & Spirituality`,
    'IAB23-1': $localize`Alternative Religions`,
    'IAB23-2': $localize`Atheism/Agnosticism`,
    'IAB23-3': $localize`Buddhism`,
    'IAB23-4': $localize`Catholicism`,
    'IAB23-5': $localize`Christianity`,
    'IAB23-6': $localize`Hinduism`,
    'IAB23-7': $localize`Islam`,
    'IAB23-8': $localize`Judaism`,
    'IAB23-9': $localize`Latter-Day Saints`,
    'IAB23-10': $localize`Pagan/Wiccan`,
    'IAB24': $localize`Uncategorized`,
    'IAB25': $localize`Non-Standard Content`,
    'IAB25-1': $localize`Unmoderated UGC`,
    'IAB25-2': $localize`Extreme Graphic/Explicit Violence`,
    'IAB25-3': $localize`Pornography`,
    'IAB25-4': $localize`Profane Content`,
    'IAB25-5': $localize`Hate Content`,
    'IAB25-6': $localize`Under Construction`,
    'IAB25-7': $localize`Incentivized`,
    'IAB26': $localize`Illegal Content`,
    'IAB26-1': $localize`Illegal Content`,
    'IAB26-2': $localize`Warez`,
    'IAB26-3': $localize`Spyware/Malware`,
    'IAB26-4': $localize`Copyright Infringement`
  }

  // Translations
  comm_purp_trans: string = $localize`Commercial purpose description`;
  research_trans: string = $localize`Research purpose description`;

  // Preferences checkboxes
  CP_PI: boolean = false;
  CP_BH: boolean = false;
  CP_LH: boolean = false;
  CP_I: boolean = false;
  RE_PI: boolean = false;
  RE_BH: boolean = false;
  RE_LH: boolean = false;
  RE_I: boolean = false;

  // Headers
  preferences_table: any = [
    $localize`Personal information`,
    $localize`Browsing history`,
    $localize`Location history`,
    $localize`Interests`
  ];

  subconsentsReceived: any[];

  constructor(private modalService: NgbModal, private spinner: NgxSpinnerService, private cdRef: ChangeDetectorRef, private kc_utils: KeycloakUtilsService, private http: HttpService) { }
  
  ngOnInit(): void {
    this.spinner.show()
    this.kc_utils.checkToken();
    // Set up checkboxes
    this.http.pcmGetConsents().subscribe(data => {
      this.initCheckboxes(data);
      this.spinner.hide();
      this.cdRef.detectChanges();
    });
  }

  checkboxChange($event: any, consentId: string){
    const consentChange: any = {
      'consentId': consentId,
      'active': $event.target.checked
    }
    this.http.pcmPutData(consentChange).subscribe(data =>{})    
  }

  checkboxSubconsentsChange($event: any, iabCode: string){
    console.log(iabCode)
    const consentChange: any = {
      'iabCode': iabCode,
      'active': $event.target.checked
    }
    this.http.pcmPutSubConsents(consentChange).subscribe(data =>{})    
  }

  initCheckboxes(data: any[]){
    for(let i = 0; i<data.length; i++){
      if(data[i]['consentId'] === 'sharing-browsing-history-commercial-purpose')
        this.CP_BH = data[i]['active']
      if(data[i]['consentId'] === 'sharing-browsing-history-research')
        this.RE_BH = data[i]['active']
      if(data[i]['consentId'] === 'sharing-interests-commercial-purpose')
        this.CP_I = data[i]['active']
      if(data[i]['consentId'] === 'sharing-interests-research')
        this.RE_I = data[i]['active']
      if(data[i]['consentId'] === 'sharing-location-history-commercial-purpose')
        this.CP_LH = data[i]['active']
      if(data[i]['consentId'] === 'sharing-location-history-research')
        this.RE_LH = data[i]['active']
      // TODO BROWSING HISTORY
      if(data[i]['consentId'] === 'sharing-personal-information-commercial-purpose')
        this.CP_PI = data[i]['active']
      if(data[i]['consentId'] === 'sharing-personal-information-research')
        this.RE_PI = data[i]['active'] 
    }
  }

  initSubconsentsCheckboxes(data: any[]){
    this.subconsentsReceived = [];
    // Only add tier 1 interests
    for(let i = 0; i<data.length; i++){
      if(data[i].tier === "Tier 1"){
        this.subconsentsReceived.push(data[i])
      }
    }
    // Sort it
    const sort = new Sort();
    this.subconsentsReceived.sort(sort.startSort('category', 'asc', ''))

  };

  openSubConsentsModal(content: any) { 

    this.http.pcmGetSubConsents().subscribe(data => {
      this.initSubconsentsCheckboxes(data);

      this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {
        if(result === 'ok'){}
      }, (reason) => {
          // If dismissed 
      });


    });




  }
}

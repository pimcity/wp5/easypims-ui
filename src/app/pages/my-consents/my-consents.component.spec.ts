import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyConsentsComponent } from './my-consents.component';

describe('MyConsentsComponent', () => {
  let component: MyConsentsComponent;
  let fixture: ComponentFixture<MyConsentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyConsentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyConsentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

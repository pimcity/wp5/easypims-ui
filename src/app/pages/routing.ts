import { Routes } from '@angular/router';
import { DataAggregationComponent } from './data-aggregation/data-aggregation.component';
import { PersonalDataSafeComponent } from './personal-data-safe/personal-data-safe.component';
import { PdsHomeComponent } from './personal-data-safe/pds-home/pds-home.component';
import { PersonalInfoComponent } from './personal-data-safe/personal-info/personal-info.component';
import { BrowsingHistoryComponent } from './personal-data-safe/browsing-history/browsing-history.component'
import { LocationHistoryComponent } from './personal-data-safe/location-history/location-history.component'
import { PersonalConsentManagerComponent } from './personal-consent-manager/personal-consent-manager.component'; 
import { TransparencyTagsComponent } from './transparency-tags/transparency-tags.component';
import { PrivacyPreservingAnalyticsComponent } from './privacy-preserving-analytics/privacy-preserving-analytics.component';
import { DataValuationToolsComponent } from './data-valuation-tools/data-valuation-tools.component';
import { DataTradingEngineComponent } from './data-trading-engine/data-trading-engine.component';
import { DataPortabilityControlComponent } from './data-portability-control/data-portability-control.component';
import { DataProvenanceComponent } from './data-provenance/data-provenance.component';
import { DataKnowledgeExtractionComponent } from './data-knowledge-extraction/data-knowledge-extraction.component';
import { PcmDashboardComponent } from './personal-consent-manager/pcm-dashboard/pcm-dashboard.component';
import { PcmPreferencesComponent } from './personal-consent-manager/pcm-preferences/pcm-preferences.component';
import { TtsTableComponent } from './transparency-tags/tts-table/tts-table.component';
import { UserProfilingComponent } from './user-profiling/user-profiling.component';
import { ShowAdsComponent } from './show-ads/show-ads.component';
import { HomeComponent } from './home/home.component';
import { MyDataComponent } from './my-data/my-data.component';
import { MyConsentsComponent } from './my-consents/my-consents.component';
import { SettingsComponent } from './settings/settings.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ReferralsComponent } from './referrals/referrals.component';

const Routing: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'error/404',
  },
  {
    path: 'data-aggregation', 
    component: DataAggregationComponent 
  },
  {
    path: 'personal-data-safe/pds-home', 
    component: PdsHomeComponent 
  },
  {
    path: 'personal-data-safe/personal-info', 
    component: PersonalInfoComponent 
  },
  {
    path: 'personal-data-safe/location-history', 
    component: LocationHistoryComponent 
  },
  {
    path: 'personal-data-safe/browsing-history', 
    component: BrowsingHistoryComponent 
  },
  {
    path: 'personal-data-safe', 
    component: PersonalDataSafeComponent 
  },
  {
    path: 'personal-consent-manager', 
    component: PersonalConsentManagerComponent 
  },
  {
    path: 'transparency-tags', 
    component: TtsTableComponent
  },
  {
    path: 'transparency-tags/:domain', 
    component: TransparencyTagsComponent
  },
  {
    path: 'privacy-preserving-analytics', 
    component: PrivacyPreservingAnalyticsComponent 
  },
  {
    path: 'data-valuation-tools', 
    component: DataValuationToolsComponent 
  },
  {
    path: 'data-trading-engine', 
    component: DataTradingEngineComponent 
  },
  {
    path: 'data-portability-control', 
    component: DataPortabilityControlComponent 
  },
  {
    path: 'data-provenance', 
    component: DataProvenanceComponent 
  },
  {
    path: 'data-knowledge-extraction', 
    component: DataKnowledgeExtractionComponent 
  },
  {
    path: 'personal-consent-manager/dashboard', 
    component: PcmDashboardComponent 
  },
  {
    path: 'personal-consent-manager/preferences', 
    component: PcmPreferencesComponent 
  },
  {
    path: 'user-profiling', 
    component: UserProfilingComponent 
  },
  {
    path: 'show-ads', 
    component: ShowAdsComponent 
  },
  {
    path: 'home', 
    component: HomeComponent 
  },
  {
    path: 'my-data', 
    component: MyDataComponent 
  },
  {
    path: 'my-consents', 
    component: MyConsentsComponent 
  },
  {
    path: 'settings', 
    component: SettingsComponent 
  },
  {
    path: 'privacy-policy', 
    component: PrivacyPolicyComponent 
  },
  {
    path: 'ref/:ref_id', 
    component: ReferralsComponent 
  },


];

export { Routing };

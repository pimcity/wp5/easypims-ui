import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-provenance',
  templateUrl: './data-provenance.component.html',
  styleUrls: ['./data-provenance.component.scss']
})
export class DataProvenanceComponent implements OnInit {

  datasets: [
    {
      "queryId": "QmVUyimo6kwYBTWpxMYa8aJKKYwDnohVGkedNKXcryV56f",
      "user_id": 9755,
      "buyer_id": 19,
      "filePath": "/tmp/QmVUyimo6kwYBTWpxMYa8aJKKYwDnohVGkedNKXcryV56f_1629295059492",
      "fileHash": "{\"Hash\":\"QmdBYbiGEALGcATaoZBZBdfzoUB9i73z7yYyQdNwWN4DQS\",\"Links\":[],\"Name\":\"QmVUyimo6kwYBTWpxMYa8aJKKYwDnohVGkedNKXcryV56f\",\"Size\":\"103\"}",
      "timestamp": "2021-08-18T13:57:39.743863",
      "datasetByQueryId": "{\"Hash\":\"QmdBYbiGEALGcATaoZBZBdfzoUB9i73z7yYyQdNwWN4DQS\",\"Links\":[],\"Name\":\"QmVUyimo6kwYBTWpxMYa8aJKKYwDnohVGkedNKXcryV56f\",\"Size\":\"103\"}",
      "userId": 9755
    },
    {
      "queryId": "Qmbshx7vv7UCr81GyZsthPtcLMmTDNQMToWZpyVMqnGuPW",
      "user_id": 6726,
      "buyer_id": 11,
      "filePath": "/tmp/Qmbshx7vv7UCr81GyZsthPtcLMmTDNQMToWZpyVMqnGuPW_1630521048797",
      "fileHash": "{\"Hash\":\"QmcA51TZAF1dwskvhXEHg7LBnqR26amgwkvKxVCuFYkPdm\",\"Links\":[],\"Name\":\"Qmbshx7vv7UCr81GyZsthPtcLMmTDNQMToWZpyVMqnGuPW\",\"Size\":\"174\"}",
      "timestamp": "2021-08-31T16:47:08.583946",
      "datasetByQueryId": "{\"Hash\":\"QmcA51TZAF1dwskvhXEHg7LBnqR26amgwkvKxVCuFYkPdm\",\"Links\":[],\"Name\":\"Qmbshx7vv7UCr81GyZsthPtcLMmTDNQMToWZpyVMqnGuPW\",\"Size\":\"174\"}",
      "userId": 6726
    }
  ]
  
  constructor() { }

  ngOnInit(): void {
  }

}

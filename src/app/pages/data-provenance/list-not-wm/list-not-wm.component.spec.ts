import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNotWmComponent } from './list-not-wm.component';

describe('ListNotWmComponent', () => {
  let component: ListNotWmComponent;
  let fixture: ComponentFixture<ListNotWmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListNotWmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNotWmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

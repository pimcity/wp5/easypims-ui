import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWmComponent } from './list-wm.component';

describe('ListWmComponent', () => {
  let component: ListWmComponent;
  let fixture: ComponentFixture<ListWmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListWmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

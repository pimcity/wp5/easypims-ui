import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor(private router: Router) { }

  privacy_policy: any = {
    'Title': $localize`POLITECNICO DI TORINO – Department of Electronics and Telecommunications`,
    'Privacy_policy': $localize`Privacy Policy`,
    'Last_revised': $localize`Last revised on 12/04/2022 `,
    'Intro': $localize`Please read this privacy policy carefully so that you fully understand how we collect, use and store information about you. In case you accept this privacy policy, we assume that you understand and agree with all the details provided below. Occasionally, we may update this privacy policy, so please kindly visit our website to always find the latest version. We will contact you in case there will be important changes.`,
    '1': $localize`Introduction`,
    '1.1': $localize`POLITECNICO DI TORINO, Department of Electronics and Telecommunications a public University in Italy, with registered addressed at Corso Duca degli Abruzzi 24, 10129, Torino (further as ‘the Controller’ or ‘we’) protects your information and your privacy and processes your personal data following the requirements provided by the relevant laws.`,
    '1.2': $localize`This privacy policy (further as ‘the Privacy Policy’) describes how we process your personal data. In particular, this Privacy Policy provides details on:`,
    '1.2.1': $localize`which personal data (further as data) of yours we collect (categories);  `,
    '1.2.2': $localize`who can you contact in case of any questions or complaints concerning your data (our contacts and contacts of our data protection officer);`,
    '1.2.3': $localize`why do we process your data (purposes);  `,
    '1.2.4': $localize`what legal grounds do we have to process your data (legal bases and legitimate interests);`,
    '1.2.5': $localize`who else will receive your data (recipients);  `,
    '1.2.6': $localize`for how long we will store your data (period);  `,
    '1.2.7': $localize`your rights.  `,
    '1.3': $localize`We value privacy and are therefore committed to protect the (personal) data of all our stakeholders with the greatest possible care, and to process personal data only in a fair and lawful manner. This Privacy Statement is applicable to our customers, partners and website visitors for the personal data collected and processed by through this website and the related services.  `,
    '1.4': $localize`This Privacy Policy contains essential information on how, as the data controller, collects and processes personal data, for what purposes and explains your rights as a data subject.  `,
    '2': $localize`Which information do we collect?`,
    '2.1': $localize`We collect and can process the following data: `,
    'data_types': [ $localize`Full name`,
      $localize`Mail address`,
      $localize`Date of birth`,
      $localize`Country you live in`,
      $localize`City you live in`,
      $localize`Gender`,
      $localize`Household members`,
      $localize`Phone number`,
      $localize`Income`,
      $localize`Job`,
      $localize`Education`,
      $localize`Location history`,
      $localize`Browsing history`,
      $localize`User interests based on your location and browsing history`],
    '2.1.1': $localize`In addition, we also process various documents submitted by you or concerning you and your communications to us in order to process and handle your queries, requests and complaints, and to engage into legal proceedings in case that would appear necessary.`,
    '3': $localize`Why do we collect information about you?`,
    '3.1': $localize`We process your data based on three legal bases: consent (art. 6(1)(a) of the GDPR, contract (art. 6(1)(c) of the GDPR and our legitimate interests (art. 6(1)(f) of the GDPR`,
    '3.2': $localize`The following data is used for the specified purposes and based on the specified legal basis:`,
    'headers': 
      [ $localize`Why do you collect my information?`,
      $localize`Which information do you collect about me?`,
      $localize`Why are you legally allowed to collect my information?`,
      $localize`How long do you keep information about me?`],

    'rows': [
      {
        '1':$localize`To process and handle your queries, requests and complaints, and to engage in legal proceedings in case that would appear necessary  `,
        '2':$localize`Name, surname, contact details you contacted us through, the information included in the query, request or complaint.`,
        '3':$localize`Based on our legitimate interest in handling your inquiries, art 6, 1, f) GDPR  `,
        '4':$localize`For three years after the project ended (approx. August 2025)`
      },
      {
        '1':$localize`To inform you about policy changes, inform you in case you win a prize, contact you to participate in surveys, and in general contact you in case of need  `,
        '2':$localize`Name, surname and e-mail address  `,
        '3':$localize`Based on contract when you sign-up for our platform, art. 6, 1, c) GDPR  `,
        '4':$localize`For three years after the project ended (approx. August 2025)`
      },
      {
        '1':$localize`For academic research such as data analyses, generation of participation insight (note that anonymisation and aggregation of data is applied whenever possible).`,
        '2':$localize`Date of birth, Country you live in, City you live in, Gender, Household members, Phone number, Income, Job, Education  `,
        '3':$localize`Based on your consent (by voluntarily submitting it and giving granular consent to sharing it), art. 6, 1, a) GDPR  `,
        '4':$localize`For three years after the project ended (approx. August 2025) or unless you revoke your consent earlier  `
      },
      {
        '1':$localize`For advertisement on the website/platform  `,
        '2':$localize`Date of birth, Country you live in, City you live in, Gender, Household members, Phone number, Income, Job, Education.`,
        '3':$localize`Based on your consent (by voluntarily submitting it and giving granular consent to sharing it), art. 6, 1, a) GDPR  `,
        '4':$localize`For three years after the project ended (approx. August 2025) or unless you revoke your consent earlier  `
      },
      {
        '1':$localize`Creating individual profiles and showing targeted advertisements. We also share this profile and the resulting data to third parties for receiving offers from data buyers  `,
        '2':$localize`Location history, browser history.  `,
        '3':$localize`Based on your consent (by voluntarily uploading it), art. 6, 1, a) GDPR`,
        '4':$localize`For three years after the project ended (approx. August 2025) or unless you revoke your consent earlier`
      }     
    ],
    '4': $localize`Which information do you have to provide and why?  `,
    '4.1': $localize`You should provide us with the information that is necessary to handle your complaints, queries and requests. In case you would not provide us with such information, we would not be able to handle them.  `,
    '5': $localize`Is your information shared with anyone else?`,
    '5.1': $localize`The whole EasyPIMS platform is to let users to freely control which of their personal data can be shared with data buyers. Data buyers can ask to get an audience of users that satisfy certain constraints (e.g., their profile and interests match the interests of the data buyer). Only users that provided their explicit consent can be part of an audience.  `,
    '5.2': $localize`EasyPIMS is part of the Project PIMCity, funded by the European Commission (EC). While we are not going to share any of your data with the EC, we will use your personal data to elaborate statistics about the users that participate in the experimental part of the project. Only users who gave their consent to use their data for research will be included in the analysis.`,
    '5.3': $localize`We may also share your data to courts and other institutions or subjects when it is required by law.  `,
    '5.4': $localize`Other than as set out in this Privacy Policy, we shall not disclose your data to any third parties without obtaining your prior explicit consent unless that would be required by law. `,
    '6': $localize`For how long do we keep your information?  `,
    '6.1': $localize`We will not retain your personal data longer than strictly necessary for the realization of the purpose of the platform. We as a project have to commit to store data for three years after the project ends in August 2022. As such, we will store your data for three years until after the end of the project, i.e. August 2025.  `,
    '7': $localize`How do we secure your information?  `,
    '7.1': $localize`All data are stored in a secure server running in a data centre in Fastweb premises. All data are backed up in a separate server in the same premises. We do not have physical access to the servers, and they are reachable only using a dedicated access protected by a VPN secured by a two-factor authentication. The access to the database is limited to only to a subset of our personnel, after strong authentication.   `,
    '7.2': $localize`We will take appropriate administrative, technical, and organizational measures against unauthorized or unlawful processing of any data or its accidental loss, destruction or damage, access, disclosure or use.  `,
    '7.3': $localize`In the event of and following discovery or notification of a breach of the security of the data, or access by an unauthorised person, we will notify you if the breach is likely to affect your privacy.   `,
    '8': $localize`How do we manage cookies?`,
    '8.1': $localize`We only use strictly necessary cookies for the functioning of the platform.`,
    '9': $localize`What are your rights?`,
    '9.1': $localize`You can withdraw your consent given to us to process your data at any time.`,
    '9.2': $localize`You can ask us whether we process the data about you and request access to that data.`,
    '9.3': $localize`You can ask us to correct the inaccurate data about you, or you can correct this yourself in your user profile.  `,
    '9.4': $localize`You can ask us to delete the data about you and we will delete it in case there are no legal basis for us to process it.  `,
    '9.5': $localize`You can ask us to restrict processing of your data (i) in case you contest the accuracy of the data, (ii) in case the processing is unlawful and you oppose the erasure of it, (iii) in case we no longer need the data but your data is necessary for you for the legal claims, or (iv) in case you have objected to processing pursuant to Art. 21(1) of the GDPR pending the verification whether our legitimate grounds override those of yours.`,
    '9.6': $localize`You can object processing your data.`,
    '9.7': $localize`You can download (and transfer) your data directly from the platform.  `,
    '9.8': $localize`You can complain to a supervisory authority and to seek a judicial remedy.  `,
    '9.9': $localize`You can look for your country’s Data Protection Authority`,
    '10': $localize`How can you contact us?`,
    '10.1': $localize`Data Controller`,
    '10.2': $localize`POLITECNICO DI TORINO – Department of Electronics and Telecommunications, with its registered office situated at Corso Duca degli Abruzzi 24, 10129 Torino, Italy, contact via`,
    '10.3': $localize`Data Protection Officer Team`,
    '10.4': $localize`To get directly into touch with the DPO team of PIMCity, contact`
    }

  accepted: string;

  ngOnInit(): void {
    if(localStorage.getItem('privacy_policy'))
      this.accepted = 'true';
    else
      this.accepted = 'false';
  }

  accept_policy(){
    localStorage.setItem('privacy_policy', 'true')
    this.accepted = 'true';
    window.location.assign(window.location.origin+'/avatar/home');
  }
}

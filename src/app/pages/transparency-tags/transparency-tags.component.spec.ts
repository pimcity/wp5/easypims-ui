import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransparencyTagsComponent } from './transparency-tags.component';

describe('TransparencyTagsComponent', () => {
  let component: TransparencyTagsComponent;
  let fixture: ComponentFixture<TransparencyTagsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransparencyTagsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransparencyTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

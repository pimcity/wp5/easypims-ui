import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TtsTableComponent } from './tts-table.component';

describe('TtsTableComponent', () => {
  let component: TtsTableComponent;
  let fixture: ComponentFixture<TtsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TtsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TtsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

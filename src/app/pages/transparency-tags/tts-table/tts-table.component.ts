import { ChangeDetectorRef, Component, OnInit, PipeTransform } from '@angular/core';
import { PrivacyMetricList, PrivacyMetricResponse } from 'src/app/models/transparency-tags';
import { HttpService } from 'src/app/services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { TableFilterService } from 'src/app/services/table-filter.service';
import { FormControl } from '@angular/forms';
import { map } from 'rxjs/internal/operators/map';
import { startWith } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';
import { domain } from 'process';

@Component({
  selector: 'app-tts-table',
  templateUrl: './tts-table.component.html',
  styleUrls: ['./tts-table.component.scss']
})
export class TtsTableComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService, private http: HttpService, private cd: ChangeDetectorRef) {}
  
  auth_token: string;

  // Pagination
  page = 1;
  pageSize = 5;
  collectionSize = 5;

  rows: any = [];
  view_trans = $localize`View`;
  filter_text: any = '';
  receivedData: any = [];
  receivedDataStruct: any[] = [];
  showTables: boolean = false;
  // filter = new FormControl('');

  scores_trans: string = $localize`
  Security Score: describes how secure the service is based on the information we obtained.\n
  Privacy Score: describes how privacy-friendly the service is based on the information we obtained.\n
  Transparency Score: describes how transparent the service is in providing information to the users.\n`


  ngOnInit(): void {
    this.spinner.show();
    this.http.ttGetPrivacyMetricsNames('20', '1').subscribe(data => {
    console.log('data', data)

    // THIS IS COMMENTED BECAUSE AT THE CURRENT STATE THERE IS NO SENSE TO RECEIVE THE DOMAINS OF DATA BUYERS, WE ARE ONLY SHOWING BROWSING HISTORY
/*
      this.receivedDataStruct = data;
      for(let i = 0; i<this.receivedDataStruct.length; i++){
        
        this.receivedData.push(
          {
            domain: this.receivedDataStruct[i][0],
            company: this.receivedDataStruct[i][2],
            categories: this.receivedDataStruct[i][3],
            scores: this.roundScores(this.receivedDataStruct[i][4]),
            type: $localize`Data buyer`,
            id: i
          }
        )

          
      }
*/
      this.http.pdsGetPersonalData('browsing-history').subscribe(data => {
        let domains: any = [];
        for(let i = 0 ; i<data.data.length; i++){
          if(!domains.includes(data.data[i].value.url)){
            domains.push(data.data[i].value.url);
            this.receivedData.push({
              domain: data.data[i].value.url,
              company: 'Not available',
              scores: {
                privacy_score: 0,
                security_score: 0,
                transparency_score: 0
              },
              categories: ['Not available'],
              type: $localize`Browsing`,
            })
          }
        }
        this.collectionSize = this.receivedData.length;
        this.spinner.hide();
        this.showTables = true;
        this.refreshTable();
        this.cd.detectChanges();
      });
    });
}
  roundScores(scores: any): { privacy_score: number; security_score: number; transparency_motivation: string[]; transparency_score: number; } {
    scores.privacy_score = Math.round(scores.privacy_score);
    scores.security_score = Math.round(scores.security_score);
    scores.transparency_score = Math.round(scores.transparency_score);
    return scores;
  }

  refreshTable(){
    this.rows = this.receivedData;
    this.rows = this.orderData();

    // Used for the pagination
    if(!this.filter_text){
      this.rows = this.rows.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    }
    this.rows = this.rows.slice(0, (1 - 1) * this.pageSize + this.pageSize);
    this.cd.detectChanges();
  }


  orderData(){
    let text = this.filter_text;
    return this.rows.filter((row: { domain: string; company: any; categories: any; type: any; }) => {
      const term = text.toLowerCase();
      return row.domain.toLowerCase().includes(term)
          || row.company.toLowerCase().includes(term)
          || row.type.toLowerCase().includes(term)
          || row.categories[0].toLowerCase().includes(term)
    });
  }


}

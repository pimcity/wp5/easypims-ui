import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { PrivacyMetricResponse } from './../../models/transparency-tags'
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinner, NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-transparency-tags',
  templateUrl: './transparency-tags.component.html',
  styleUrls: ['./transparency-tags.component.scss']
})
export class TransparencyTagsComponent implements OnInit {

  auth_token: string;
  receivedData: any = {};
  third_parties_preview: string[];
  domain: string;
  tt_exists: string = 'loading';
  
  constructor(private spinner: NgxSpinnerService, private activated_route: ActivatedRoute, private modalService: NgbModal, private http: HttpService, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.spinner.show()
    this.activated_route.paramMap.subscribe(params => { 
      this.domain = params.get('domain')!; 
        this.http.ttGetPrivacyMetrics(this.domain).subscribe(data => {
          this.receivedData = data;
          console.log('TT received data ',this.receivedData)

          // SHOW WEB DATA THIRD PARTIES PREVIEW
          if(this.receivedData.webdata.connected_third_parties.length < 5){
            this.third_parties_preview = this.receivedData.webdata.connected_third_parties;
          }
          else {
            this.third_parties_preview = this.receivedData.webdata.connected_third_parties
            .slice(0, 5);
          }
          this.tt_exists = 'yes';
          this.spinner.hide();
          this.cd.detectChanges();
        },
        (error => {
          if(error.status = 404){
            this.tt_exists = 'no';
            this.spinner.hide();
            this.cd.detectChanges();
          }
        }))
    });

  }

  informationProvidedModal(content: any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {
      if(result === 'done'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }

  webdataModal(content: any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg'}).result.then((result) => {
      if(result === 'done'){
        // If delete has been presed
      }
    }, (reason) => {
        // If dismissed 
    });
  }
}

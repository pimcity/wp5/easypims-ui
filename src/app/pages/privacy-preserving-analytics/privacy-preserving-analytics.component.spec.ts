import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyPreservingAnalyticsComponent } from './privacy-preserving-analytics.component';

describe('PrivacyPreservingAnalyticsComponent', () => {
  let component: PrivacyPreservingAnalyticsComponent;
  let fixture: ComponentFixture<PrivacyPreservingAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyPreservingAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyPreservingAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

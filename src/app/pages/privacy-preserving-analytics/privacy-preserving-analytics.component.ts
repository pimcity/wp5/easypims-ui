import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-privacy-preserving-analytics',
  templateUrl: './privacy-preserving-analytics.component.html',
  styleUrls: ['./privacy-preserving-analytics.component.scss']
})
export class PrivacyPreservingAnalyticsComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder) { 
    this.form = this.fb.group({
      userChoiceIndexes: this.fb.array([]),
    });
  }

  ngOnInit(): void {
  }

  adduserChoiceIndexes() {
    const creds = this.form.controls.userChoiceIndexes as FormArray;
    creds.push(this.fb.group({
      userChoiceIndex: '',
    }));
  }
}

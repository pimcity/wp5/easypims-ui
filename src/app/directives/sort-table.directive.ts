import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { Sort } from '../utils/sort';

@Directive({
  selector: '[appSortTable]'
})
export class SortTableDirective {

  @Input() appSortTable: Array<any>;
  constructor(private renderer: Renderer2, private targetElem: ElementRef) { }


  @HostListener("click")
  sortData() {
    // Create Object of Sort Class
    const sort = new Sort();
    // Get Reference Of Current Clicked Element
    const elem = this.targetElem.nativeElement;
    // Get In WHich Order list should be sorted by default it should be set to desc on element attribute
    const order = elem.getAttribute("data-order");
    // Get The Property Type specially set [data-type=date] if it is date field
    const type = elem.getAttribute("data-type");
    // Get The Property Name from Element Attribute
    const property = elem.getAttribute("data-name");
    
    const up_arrow = this.renderer.createElement('i');
    const down_arrow = this.renderer.createElement('i');
    up_arrow.setAttribute('class', 'las la-arrow-up fs-2 ps-3 ms-3 ')
    down_arrow.setAttribute('class', 'las la-arrow-down fs-2 ps-3 ms-3 ')
    // this.renderer.removeChild(elem, up_arrow);
    // this.renderer.removeChild(elem, down_arrow);

    for (let child of elem.children) {
      this.renderer.removeChild(elem.children, child);
    }


    if (order === "desc") {
      this.appSortTable.sort(sort.startSort(property, order, type));
      this.renderer.appendChild(elem, down_arrow);
      elem.setAttribute("data-order", "asc");
    }
    else {
      this.appSortTable.sort(sort.startSort(property, order, type));
      this.renderer.appendChild(elem, up_arrow);

      elem.setAttribute("data-order", "desc");
    }

  }
}

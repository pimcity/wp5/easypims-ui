import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableFilterService {

  constructor() { }



  tableSearch(input_id: string, table_id: string, table_header_id: string) {
    var input, filter, found, table, tr, td, i, j;
    input = document.getElementById(input_id);
    filter = (<HTMLInputElement>input).value.toUpperCase();
    table = document.getElementById(table_id);
    tr = table!.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                found = true;
            }
        }
        if (found) {
            tr[i].style.display = "";
            found = false;
        } else {
            if (tr[i].id != table_header_id){tr[i].style.display = "none";}
        }
    }
  }

}

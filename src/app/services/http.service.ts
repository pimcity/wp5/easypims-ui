import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ChangeConsent } from '../models/personal-consent-manager';
import { Metadata } from '../models/data-aggregation';
import { DataExportPost, DataSource, DataSourcePost, DataTransformationPost } from '../models/data-portability-control';
import { format } from 'path';
import { Binary } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {

   }


  //  SHOW ADS
  public saGetAd(): Observable<any>{
    const current_token = localStorage.getItem('pdaAccessToken');
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + current_token,
      'Content-Type': 'text/plain'
    });

    const params = {
      h: '600',
      w: '300',
      munAd: '1'
    }

    return this.httpClient.get(environment.SaApiUrl + '/getAd', {headers: headers, params: params, responseType:'text'})
  }


  //  PERSONAL DATA SAFE (PDS)

  public pdsPostToken(username: string, password: string): Observable<any>{
    const body_object = {
      username: username,
      password: password
    };
    return this.httpClient.post(environment.PdsApiUrl + '/token/', body_object)
  }

  public pdsPostLocationHistory(file: File): Observable<any>{

    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });

    let formData = new FormData();
    formData.append('zip', file);
    formData.append('group_names', 'location-history');

    
    return this.httpClient.post(environment.PdsApiUrl + '/v1/personal-data/zip-file/', formData, { 
      headers: headers,
      reportProgress: true,
      observe: 'events'})
  };

  public pdsGetGraphData(): Observable<any>{
    const current_token = localStorage.getItem('pdaAccessToken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + current_token
    });
    return this.httpClient.get(environment.PdsApiUrl + '/v1/utility/graph-data', { headers: headers})
  }

  public pdsGetMacroGroups(): Observable<any>{
    const current_token = localStorage.getItem('pdsAccessToken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + current_token
    });
    return this.httpClient.get(environment.PdsApiUrl + '/v1/utility/macro-groups', { headers: headers})
  }

  public pdsGetPersonalData(category: string): Observable<any>{
    const current_token = localStorage.getItem('pdaAccessToken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + current_token
    });
    
    const params = {
      category: category
    }

    return this.httpClient.get(environment.PdsApiUrl + '/v1/personal-data', { headers: headers, params: params})
  }

  public pdsPostPersonalData(form: any): Observable<any>{
    const current_token = localStorage.getItem('pdaAccessToken');

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + current_token

    });
    
    const form_fields = Object.keys(form);
    const data = [];

    for(let i = 0; i<form_fields.length; i++){
      let field = form_fields[i]
      let value = form[field]
      if(value !== null){
        data.push(     
          {
            "value": {
              [field]: value
            },
            "metadata": field,
            "type": field,
            "group-name": "personal-information",
            "description": ""
          }
        );
      }
    }
      return this.httpClient.post(environment.PdsApiUrl + '/v1/personal-data/batch/', data, {headers: headers})
  }

  public pdsDeletePersonalData(id: string){
    const current_token = localStorage.getItem('pdaAccessToken');

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + current_token
    });

    return this.httpClient.delete(environment.PdsApiUrl + '/v1/personal-data/' + id, {headers: headers})
  }





  // PERSONAL CONSENT MANAGER

  public pcmGetConsents(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')   
    });
    
    return this.httpClient.get(environment.PcmApiUrl + '/consents', {headers: headers})
  }

  public pcmGetSubConsents(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')   
    });
    
    return this.httpClient.get(environment.PcmApiUrl + '/consents/iab', {headers: headers})
  }

  public pcmGetDataShared(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ' + current_token
    });
    
    return this.httpClient.get(environment.PcmApiUrl + '/consents/data-sharing', {headers: headers})
  }

  public pcmPutData(body: ChangeConsent): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });
    
    return this.httpClient.put(environment.PcmApiUrl + '/consents', body, {headers: headers})
  } 

  public pcmPutSubConsents(body: any): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });
    
    return this.httpClient.put(environment.PcmApiUrl + '/consents/iab', body, {headers: headers})
  } 

  public pcmPutDataShared(body: ChangeConsent): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ' + current_token
    });
    
    return this.httpClient.put(environment.PcmApiUrl + '/consents/data-sharing', body, {headers: headers})
  } 

  // DATA PORTABILITY CONTROL
  
  // public dpcPostRequestAuthToken(): Observable<any>{
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/x-www-form-urlencoded'
  //   });
    
  //   const body = new HttpParams()
  //   .set('grant_type', 'client_credentials')
  //   .set('client_id', 'dpc')
  //   .set('client_secret', environment.DpcSecret)

  //   return this.httpClient.post(environment.EasyPimsUrl + '/identity/auth/realms/pimcity/protocol/openid-connect/token', body, {headers: headers})
  // }

  // public dpcGetDatasources(): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });
    
  //   return this.httpClient.get(environment.DpcApiUrl + '/datasources', {headers: headers})
  // }

  // public dpcGetDataExports(): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });
    
  //   return this.httpClient.get(environment.DpcApiUrl + '/dataexports', {headers: headers})
  // }

  // public dpcGetDataTransformations(): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });
    
  //   return this.httpClient.get(environment.DpcApiUrl + '/datatransformations', {headers: headers})
  // }

  // public dpcPostDatasource(datasource: DataSourcePost): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });

  //   return this.httpClient.post<DataSourcePost>(environment.DpcApiUrl + '/datasources', datasource, {headers: headers})
  // }

  // public dpcDeleteDatasource(id: string): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });

  //   return this.httpClient.delete(environment.DpcApiUrl + '/datasources/'+ id, {headers: headers})
  // }

  // public dpcPatchDatasource(id: string, enabled: boolean): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    

  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'accept':'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });
    
  //   const params = {
  //     enabled: enabled
  //   };
    
  //   return this.httpClient.patch(environment.DpcApiUrl + '/datasources/'+ id, params, {headers: headers})
  // }

  // public dpcPostDataTransformation(data_transformation: DataTransformationPost): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });

  //   return this.httpClient.post<DataTransformationPost>(environment.DpcApiUrl + '/datatransformations', data_transformation, {headers: headers})
  // }

  // public dpcDeleteDataTransformation(id: string): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });

  //   return this.httpClient.delete(environment.DpcApiUrl + '/datatransformations/'+ id, {headers: headers})
  // }

  // public dpcPatchDataTransformation(id: string, enabled: boolean): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    

  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'accept':'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });
    
  //   const params = {
  //     enabled: enabled
  //   };
    
  //   return this.httpClient.patch(environment.DpcApiUrl + '/datatransformations/'+ id, params, {headers: headers})
  // }

  // public dpcPostDataExport(data_export: DataTransformationPost): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });

  //   return this.httpClient.post<DataExportPost>(environment.DpcApiUrl + '/dataexports', data_export, {headers: headers})
  // }

  // public dpcDeleteDataExport(id: string): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    
    
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });

  //   return this.httpClient.delete(environment.DpcApiUrl + '/dataexports/'+ id, {headers: headers})
  // }

  // public dpcPatchDataExport(id: string, enabled: boolean): Observable<any>{
  //   const token = localStorage.getItem('dpcAuthToken');    

  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'accept':'application/json',
  //     'Authorization': 'Bearer ' + token
  //   });
    
  //   const params = {
  //     enabled: enabled
  //   };
    
  //   return this.httpClient.patch(environment.DpcApiUrl + '/dataexports/'+ id, params, {headers: headers})
  // }

  // PRIVACY METRICS (TRANSPARENCY TAGS)


  public ttGetPrivacyMetricsNames(limit: string, offset: string): Observable<any>{
    const token = localStorage.getItem('pdaAccessToken');    
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    
    const params = {
      limit: limit,
      offset: offset
    };

    return this.httpClient.get(environment.TtApiUrl + '/privacy-metrics', {headers: headers, params: params})
  }

  public ttGetPrivacyMetrics(name: string): Observable<any>{
    const token = localStorage.getItem('pdaAccessToken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
 
    return this.httpClient.get(environment.TtApiUrl + '/privacy-metrics/' + name,  {headers: headers})
  }

  // USER PROFILING 
  public upGetProfile(token: string): Observable<any>{
    const auth_token = localStorage.getItem('pdaAccessToken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + auth_token
    });
 
    const params = {
     // token: token
    };

    return this.httpClient.get(environment.UpApiUrl + '/getProfile', {headers: headers, params: params})
  }

  // DATA AGGREGATION 

  public daGetDatasets(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ' + token
    });
 
    // const params = {
    //   token: token
    // };

    return this.httpClient.get(environment.DaApiUrl + '/datasets', {headers: headers})
  }

  public daPostDataset(metadata: Metadata, file: any): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ' + token
    });
    const body_object = {
      metadata: metadata,
      file: file
    };

    return this.httpClient.post(environment.DaApiUrl + '/add', body_object)
  }

  // TASK MANAGER
  public tmGetTasks(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });

    return this.httpClient.get(environment.TmApiUrl + '/tasks', {headers: headers})
  }

  public tmPostReferral(ref_id: string): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });
    const body_object = {
      referrerCode: ref_id
    };

    return this.httpClient.post(environment.TmApiUrl + '/referrals', body_object)
  }

  public tmGetReferral(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });

    return this.httpClient.get(environment.TmApiUrl + '/referrals', {headers: headers})
  }

  public tmGetPoints(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });

    return this.httpClient.get(environment.DteApiUrl + '/accounts/end-users/points', {headers: headers})
  }

  public tmGetSweepstakes(): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
    });

    return this.httpClient.get(environment.TmApiUrl + '/sweepstakes', {headers: headers})
  }

  
public tmGetTransactions(): Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('pdaAccessToken')
  });

  return this.httpClient.get(environment.DteApiUrl + '/market/end-users/transactions', {headers: headers})
  }
}




import { Injectable } from '@angular/core';
import { KeycloakService, KeycloakEventType } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class KeycloakUtilsService {

  constructor(private keycloakService: KeycloakService) { }

  checkToken(){
    this.keycloakService.getToken().then(data => {
      localStorage.setItem('pdaAccessToken', data)
    })
  }
}

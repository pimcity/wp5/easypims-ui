export class Profile {
    name: string;
    relevance: number;
    subCategories: any[];
}
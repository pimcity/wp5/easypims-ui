export class PrivacyMetricResponse {
    'identifier': string;
    'name': string;
    'provided_information': {
        'category': string;
        'consent_withdraw': string;
        'country': string;
        'data_controller': string;
        'data_controller_contact_address': string;
        'data_controller_contact_mail': string;
        'data_controller_contact_phone': string;
        'data_controller_identity': string;
        'data_for_automatic_decision': string;
        'data_location': string;
        'data_processed': string;
        'data_processing_legal_basis': string;
        'data_processing_purpose': string;
        'data_processor': string;
        'data_recipients': string[];
        'data_retention_period': string;
        'data_subprocessors': string[];
        'data_transfer': string;
        'declared_company_name': string;
        'dpo_contact_address': string;
        'dpo_contact_mail': string;
        'dpo_contact_phone': string;
        'user_rights': string;
        'website': string
    };
    'scores': {
        'privacy_score': number;
        'security_score': number;
        'transparency_motivation': string[];
        'transparency_score': number
    };
    'webdata': {
        'categories': string[];
        'company_name': string;
        'connected_third_parties': string[];
        'connected_websites': string[];
        'operates_under': string;
        'presence_in_security_lists': string[];
        'privacy_policy': string;
        'rank_in_category': number;
        'reach': number;
        'third_party': boolean;
        'tracking_devices': string[];
        'website': boolean;
    }
}

export class PrivacyMetricList {
    id: number;

    domain: string;
    company: string;
    categories: string[];
    scores: {
        privacy_score: number;
        security_score: number;
        transparency_motivation: string[];
        transparency_score: number
    };
}
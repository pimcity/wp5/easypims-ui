export class DataExport {
    _id: string;
    name: string;
    manifest_version: number;
    type: string;
    class: string;
    configuration: {
        sourceFile: string;
    };
    enabled: boolean;
}

export class DataSource {
  '_id': string;
  'name': string;
  'type': string;
  'class': string;
  'description': string;
  'module': string;
  'class-name': string;
  'configuration': any;
  'enabled': boolean;
}

export class DataSourcePost {
  "class": string;
  "manifest-version": number;
  "name": string;
  "type": string;
}

export class DataExportPost {
  "class": string;
  "manifest-version": number;
  "name": string;
  "type": string;
}

export class DataTransformationPost {
  "class": string;
  "manifest-version": number;
  "name": string;
  "type": string;
}

export class DataTransformation {
  _id: string;
  name: string;
  manifest_version: number;
  type: string;
  class: string;
  configuration: {
      sourceFile: string;
  };
  enabled: boolean;
}


export class QueryDetails  {
    'category': string;
    'total-objects': number;
    'filter-start': number;
    'filter-end': number;
  }

export class VisitedUrl {
    'url': string;
    'time': string;
    'title': string;
}

export class VisitedLocation {
    'time': string;
    'latitude': number;
    'longitude': number;
    'description': string;
}

export class DataBrowsingHistory {
    'id': number;
    'value': {
        'visited-url': VisitedUrl;
    }
    'metadata': string;
    'data_type': string;
    'group_name': string;
    'description': string;
    'created': string;
}

export class DataLocationHistory {
    'id': number;
    'value': {
        'visited-location': VisitedLocation;
    }
    'metadata': string;
    'data_type': string;
    'group_name': string;
    'description': string;
    'created': string;
}

export class PersonalDataBrowsingHistory {
    'query-details': QueryDetails;
    'data': DataBrowsingHistory[];
}

export class PersonalDataLocationHistory {
    'query-details': QueryDetails;
    'data': DataLocationHistory[];
}

export class TokenObject {
    'refresh': string;
    'access': string;
    'username': string;
    'user_id': number;
    'access_lifetime': string;
    'refresh_lifetime': string;
}

export class MacroGroup {
    'group_name': string;
    'total': number
}

export class DataDays {
    'day': string;
    'total': number;
}
export class GraphData {
    "result": string;
    "macro-groups": MacroGroup[];
    "data-days": DataDays[];
}

export class PersonalInfo {
    'firstname': string;
    'lastname': string;
    'birth_date': string;
    'age': string;
  };

export class PersonalDataGeneric {
    'query-details': QueryDetails;
    'data': any[];
}
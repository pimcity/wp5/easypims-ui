export class Dataset{
  queryId: string;
  user_id: number;
  buyer_id: number;
  filePath: string;
  fileHash: string;
  timestamp: string;
  datasetByQueryId: string;
  userId: number;
}
export class ReceivedDatasets {
    datasets: Dataset[];
}

export class Dataset {
    owner: string;
    createdAt: string;
    sensitive: boolean;
    name: string;
}

export class Metadata {
    dataset: string;
    owner: string;
    software: string;
    sensitive: string;
    anonymized: string;
    format: string;
    created_at: string;
    expiring_time: string;
}
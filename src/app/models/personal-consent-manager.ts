export class Data  {
    'consentId': string;
    'dataType': string;
    'purpose': string;
    'userId': string;
    'active': false;
}

export class ChangeConsent {
    "consentId": string;
    "active": boolean;
}

export class Dashboard {
    id: string;
    name: string;
    email: string;
    accountCreatedAt: number;
    offersProcessed: number;
    missedOffers: number;
    balance: number; 
}
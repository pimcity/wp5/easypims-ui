import { KeycloakOptions, KeycloakService } from 'keycloak-angular';
import { env } from 'process';
import { environment } from 'src/environments/environment';
 
export function initializeKeycloak(keycloak: KeycloakService) {
    return () =>
      keycloak.init({
        config: {
            url: 'https://easypims.pimcity-h2020.eu/identity/auth',
            // PROD KC url: 'https://identity.easypims.eu/auth',
            realm: 'pimcity',
            clientId: 'pda',
        },
        initOptions: {
          onLoad: 'login-required',
          checkLoginIframe: false  
          },
          bearerExcludedUrls: [
            environment.PdsApiUrl,
            environment.UpApiUrl,
            environment.TtApiUrl,
            environment.DpcApiUrl
          ]
      });
  }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { KeycloakService } from 'keycloak-angular';
import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-aside-menu',
  templateUrl: './aside-menu.component.html',
  styleUrls: ['./aside-menu.component.scss'],
})
export class AsideMenuComponent implements OnInit {
  appAngularVersion: string = environment.appVersion;
  appPreviewChangelogUrl: string = environment.appPreviewChangelogUrl;

  constructor(private _router: Router, private modalService: NgbModal, private keycloak: KeycloakService) {}

  privacy_policy: boolean;

  openSignOutModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then(async (result) => {
      if(result === 'confirm sign out'){
        await this.keycloak.logout().then( () => {
          window.location.reload();
        })
      }
    }, () => {
        // If dismissed 
    });
  }

  ngOnInit(): void {
    if(localStorage.getItem('privacy_policy') == 'true'){
      this.privacy_policy = true;
    }
    else{
      this.privacy_policy = false;
      this._router.navigateByUrl('/privacy-policy')
    }
  }
}

FROM node:15.14-alpine as node

RUN mkdir -p /usr/src/pda
WORKDIR /usr/src/pda
COPY package*.json ./
COPY . .
RUN npm install

CMD ["npm", "start"]
